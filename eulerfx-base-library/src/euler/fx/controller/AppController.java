/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx.controller;

import java.util.Map;
import javafx.application.Application;
import javafx.fxml.Initializable;

/**
 *
 * @author agime
 * @param <T>
 * 
 * This interface should be implemented by the FXML controllers
 * it provides methods for injecting a reference to the main Application class
 * and also provides support for setting controller parameters in the style of 
 * HTTP session parameters.
 * 
 * Furthermore, a custom initialiser is provided, this method that can be called dynamically
 * after JavaFX intialiser is called (hence all controller objects are already intialised)
 * 
 */
public interface AppController<T extends Application> extends Initializable{
    /*    
    This supports the injection of the main application in the controller
    */
    public void setApp(T application);
    /*
    Returns the main application instance
    */
    public T getApp();    
    
    /*
    This method should be used after loader.getController() is called to inject parameters in the controller.
    */
    public void setParameters(Map parameters);
    
    /*
    Some initialising operations on controllers must be executed after all controller objects are ready.
    This method can be called after loader.getController() to do that custom operations
    */
    public void customInitialiser();
	
}
