/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx.tree;

import euler.bugzilla.model.query.TreeTypeEnum;
import java.io.Serializable;
import javafx.beans.property.StringProperty;

/**
 *
 * @author agime
 */
public interface TreeElement extends Serializable{
    
    public Long getId();
    public String getName(); 
    public String getDisplayName();
    public TreeTypeEnum getType();
    public StringProperty displayNameProperty();
    
    public void setName(String name);
    
}
