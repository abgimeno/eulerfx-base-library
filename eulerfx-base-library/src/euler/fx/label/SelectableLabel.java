/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx.label;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.scene.control.IndexRange;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;

/**
 *
 * @author agime
 */
public class SelectableLabel extends Labeled {

    private int prefColumnSize = 26;
    private final String defaultStyle = "-fx-background-color: transparent;";

    public SelectableLabel() {
        //this.setStyle(defaultStyle);
        //this.prefColumnCountProperty().set(prefColumnSize);
    }

    public SelectableLabel(String string) {
        super(string);
        //this.setStyle(defaultStyle);
        //this.prefColumnCountProperty().set(string.length() / 2);
    }
    /**
     * The current selection.
     */
    private ReadOnlyObjectWrapper<IndexRange> selection = new ReadOnlyObjectWrapper<IndexRange>(
            this, "selection", new IndexRange(0, 0));

    public final IndexRange getSelection() {
        return selection.getValue();
    }

    public final ReadOnlyObjectProperty<IndexRange> selectionProperty() {
        return selection.getReadOnlyProperty();
    }
    /**
     * Defines the characters in the TextInputControl which are selected
     */
    private ReadOnlyStringWrapper selectedText = new ReadOnlyStringWrapper(
            this, "selectedText");

    public final String getSelectedText() {
        return selectedText.get();
    }

    public final ReadOnlyStringProperty selectedTextProperty() {
        return selectedText.getReadOnlyProperty();
    }
}
