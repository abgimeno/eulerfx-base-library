/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.fx.table.cell;

import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author agime
 * @param <T>
 * Transforms a list into a comma separated string
 * 
 */
public class ClickableListCell<T> extends TableCell<T, List>{

    public ClickableListCell(EventHandler<MouseEvent> mouseClickedEvent) {
        super();
        addEventFilter(MouseEvent.MOUSE_CLICKED, mouseClickedEvent);
    }

    @Override
    protected void updateItem(List t, boolean empty) {
        super.updateItem(t, empty); //To change body of generated methods, choose Tools | Templates.
        if(empty){// this removes old rows and custom CSS styles when new items has less rows than the old items stored in the table
            setText(null);
            setStyle(null);
            setGraphic(null);
        }
        if (t != null) {
            StringBuilder concatenateList = new StringBuilder();            
            for(Object o : t){
                concatenateList.append(o.toString());
                if(t.indexOf(o) != (t.size()-1)){
                    concatenateList.append(", ");
                }
            }
            setText(concatenateList.toString());
         }
    }
    
}
