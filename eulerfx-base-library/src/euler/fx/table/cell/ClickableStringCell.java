/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx.table.cell;

import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author agime
 * @param <T>
 */
public class ClickableStringCell<T> extends TableCell<T, String> {

    public ClickableStringCell(EventHandler<MouseEvent> mouseClickedEvent) {
        super();
        addEventFilter(MouseEvent.MOUSE_CLICKED, mouseClickedEvent);
    }

    @Override
    protected void updateItem(String t, boolean empty) {
        super.updateItem(t, empty); //To change body of generated methods, choose Tools | Templates.
        
        if(empty){// this removes old rows and custom CSS styles when new items has less rows than the old items stored in the table
            setText(null);
            setStyle(null);
            setGraphic(null);
        } 
        if (t != null) {
            setText(t);            
        }

    }
}
