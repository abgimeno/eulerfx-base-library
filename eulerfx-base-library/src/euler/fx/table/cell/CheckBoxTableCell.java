/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx.table.cell;

import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;

/**
 *
 * @author agime
 */
public class CheckBoxTableCell<S, T> extends TableCell<S, T> {

        private final CheckBox checkBox;
        private ObservableValue<T> ov;

        public CheckBoxTableCell() {
            checkBox = new CheckBox();
            checkBox.setAlignment(Pos.CENTER);
            checkBox.setSelected(false);
            setAlignment(Pos.CENTER);
            setGraphic(checkBox);           
        }

        @Override
        public void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);            
            if (empty) {
                setText(null);
                setGraphic(null);
              
            } else {
                setGraphic(checkBox);
                if (ov instanceof BooleanProperty) {
                    checkBox.selectedProperty().unbindBidirectional((BooleanProperty) ov);
                }                   
               ov = getTableColumn().getCellObservableValue(getIndex());
                if (ov instanceof BooleanProperty) {
                    checkBox.selectedProperty().bindBidirectional((BooleanProperty) ov);
                }
                if (item instanceof Boolean){
                    checkBox.setSelected((Boolean)item);
                }
            }
        }
    }
