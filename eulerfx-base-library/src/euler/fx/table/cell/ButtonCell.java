/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx.table.cell;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.Node;

/**
 *
 * @author agime
 */
public class ButtonCell {      
   
   private List<Node> buttons= new ArrayList<>();
   
    
    public ButtonCell() {
        
    }
            
    public ButtonCell(Node node) {
        this.buttons.add(node);
    }

    public ButtonCell (List<Node> nodes){
        this.buttons = nodes;
    }

    public void addNode (Node node){        
        this.buttons.add(node);
    }
    public Node getNode(int i){
        return this.buttons.get(i);
    }
}
