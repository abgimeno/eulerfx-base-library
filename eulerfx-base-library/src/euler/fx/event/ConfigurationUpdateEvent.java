/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.fx.event;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author agime
 */
public class ConfigurationUpdateEvent extends Event{
    
    public static final EventType<ConfigurationUpdateEvent> PRODS_SET = new EventType<>(Event.ANY, "PRODS_SET");
    public static final EventType<ConfigurationUpdateEvent> LEGALVALUES_SET = new EventType<>(Event.ANY, "LEGALVALUES_SET");
    public static final EventType<ConfigurationUpdateEvent> USERS_SET = new EventType<>(Event.ANY, "USERS_SET");
        
    private static final EventType<ConfigurationUpdateEvent> NONE = new EventType<>(Event.ANY, "NONE");
   

    public ConfigurationUpdateEvent(EventType<? extends Event> et) {
        super(et);
    }

    public ConfigurationUpdateEvent(Object o, EventTarget et, EventType<? extends Event> et1) {
        super(o, et, et1);
    }
    

    @Override
    public ConfigurationUpdateEvent copyFor(Object o, EventTarget et) {
        return (ConfigurationUpdateEvent)super.copyFor(o, et); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EventType<? extends ConfigurationUpdateEvent> getEventType() {
        return (EventType<? extends ConfigurationUpdateEvent>)super.getEventType(); //To change body of generated methods, choose Tools | Templates.
    }

}
