/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.user;

import euler.bugzilla.model.BugzillaUser;
import euler.bugzilla.j2bugzilla.enums.UserInfoParams;
import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author agime
 */
public class UserInfo implements BugzillaMethod{
    /**
     * The method Bugzilla will execute via XML-RPC
     */
    private static final String METHOD_NAME = "User.get";
    
    /**
     * A {@literal Map} returned by the XML-RPC method.
     */
    private Map<Object, Object> hash = new HashMap<>();
    
    /**
     * A {@literal Map} used by the XML-RPC method containing the required object
     * parameters.
     */
    private final Map<Object, Object> params = new HashMap<>();

    /**
     * Creates a new {@link UserInfo} object to query the Bugzilla installation
     * based on one or more {@link SearchQuery SearchQueries}.
     *
     * @param parameters
     */
    public UserInfo(Map<UserInfoParams, List<String>> parameters) {
        if (parameters.isEmpty()) {
            throw new IllegalArgumentException("At least one search query is required");
        }
        for(UserInfoParams uip: parameters.keySet()){
            params.put(uip.getType(), parameters.get(uip));
        }
        

    }
    public List<BugzillaUser> getResults() {

        List<BugzillaUser> results = new ArrayList<>();
        /*
         * The following is messy, but necessary due to how the returned XML document nests
         * Maps.
         */
        if (hash.containsKey("users")) {
            Object[] matches = (Object[]) hash.get("users");
            for (Object o : matches) {
                @SuppressWarnings("unchecked")
                Map<String, Object> bugMap = (HashMap<String, Object>) o;
                //Handle version property for older Bugzillas which did not include it in the public portion of the hash
                /*if (!bugMap.containsKey("version")) {
                    Map<?, ?> internals = (Map<?, ?>) bugMap.get("internals");
                    bugMap.put("version", internals.get("version"));
                }*/
                BugzillaUser user = new MyBugzillaUserFactory().createUser(bugMap);
                results.add(user);
            }
        }
        return results;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setResultMap(Map<Object, Object> hash) {
        this.hash = hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Object, Object> getParameterMap() {
        return params;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMethodName() {
        return METHOD_NAME;
    }
    
}
