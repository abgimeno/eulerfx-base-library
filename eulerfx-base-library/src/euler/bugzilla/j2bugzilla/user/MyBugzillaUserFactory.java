/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.user;

import euler.bugzilla.model.BugzillaUser;
import euler.bugzilla.j2bugzilla.enums.BugzillaUserParams;
import java.util.Arrays;
import java.util.Map;

/**
 *
 * @author agime
 */
public class MyBugzillaUserFactory {

    public MyBugzillaUserFactory() {
    }

    public BugzillaUser createUser(Map<String, Object> userMap) {
        /*
        the below is quick dirty fix, need to support map
        within bugzillauser as for buginfo
        */
        BugzillaUser bu = new BugzillaUser();
        if(userMap.containsKey(BugzillaUserParams.ID.getType())){
            bu.setId((Integer) userMap.get(BugzillaUserParams.ID.getType()));
        }
        if(userMap.containsKey(BugzillaUserParams.NAME.getType())){
            bu.setName((String) userMap.get(BugzillaUserParams.NAME.getType()));
        }
        if(userMap.containsKey(BugzillaUserParams.REAL_NAME.getType())){
            bu.setRealName((String) userMap.get(BugzillaUserParams.REAL_NAME.getType()));
        }
        if(userMap.containsKey(BugzillaUserParams.EMAIL.getType())){
            bu.setEmail((String) userMap.get(BugzillaUserParams.EMAIL.getType()));
        }
        if(userMap.containsKey(BugzillaUserParams.ENABLED.getType())){
            bu.setCanLogin((Boolean) userMap.get(BugzillaUserParams.ENABLED.getType()));
        }
        if(userMap.containsKey(BugzillaUserParams.GROUPS.getType())){
            Object[] groups = (Object[]) userMap.get(BugzillaUserParams.GROUPS.getType());        
            bu.setGroups(Arrays.asList(groups));
        }                                
        return bu;
    }
    
}
