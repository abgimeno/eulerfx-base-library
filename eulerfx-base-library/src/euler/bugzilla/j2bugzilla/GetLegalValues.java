/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla;

/**
 *
 * @author agime
 */
import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import euler.bugzilla.j2bugzilla.base.Product;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The {@literal GetLegalValues} class allows clients to query their installation
 * for information on the allowed values for fields in bug reports, which may be
 * edited to be installation-specific. For example, the default workflow has
 * changed from Bugzilla 3.x to 4.x. Additionally, custom priority and severity
 * values may be defined.
 *
 * @author Tom
 *
 */
public class GetLegalValues implements BugzillaMethod {

    private static final String METHOD_NAME = "Bug.fields";
    private final Map<Object, Object> params = new HashMap<>();

    private Set<String> legalValues = Collections.emptySet();
    private Product product = null;

    public GetLegalValues(String field) {
        params.put("names", field);
    }

    public GetLegalValues(String field, Product product) {
        this(field);
        this.product = product;
    }

    public Set<String> getLegalValues() {
        return legalValues;
    }

    @Override
    public void setResultMap(Map<Object, Object> hash) {
        legalValues = new HashSet<>();

        Object[] array = (Object[]) hash.get("fields");
        @SuppressWarnings("unchecked")//Cast to structure defined by webservice
        Map<Object, Object> fields = (Map<Object, Object>) array[0];

        Object[] values = (Object[]) fields.get("values");
        for (Object obj : values) {
            @SuppressWarnings("unchecked")//Cast to structure defined by webservice
            Map<Object, Object> map = (Map<Object, Object>) obj;
            String name = (String) map.get("name");

            if (product == null) {
                legalValues.add(name);
            } else {
                //check the 'visibility_values' field
                Object[] productNames = (Object[]) map.get("visibility_values");
                for (Object curr : productNames) {
                    if (curr.equals(product.getName())) {
                        legalValues.add(name);
                        break;
                    }

                }
            }
        }
    }

    @Override
    public String getMethodName() {
        return METHOD_NAME;
    }

    @Override
    public Map<Object, Object> getParameterMap() {
        return Collections.unmodifiableMap(params);
    }
}
