/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.base;

import euler.bugzilla.model.*;
import java.util.Date;


public class Comment{

    /**
     * The unique ID of this {@link Comment} on a Bugzilla installation
     */
    private final int id;

    /**
     * The message content of this {@link Comment}
     */
    private  final String text;
    private  final String author;
    private  final int bugId;
    private  final Date time;    

    
    /**
     * Creates a new {@link Comment} from a Bugzilla installation
     *
     * @param id The unique ID of this comment
     * @param text The text content of this comment
     * @param author
     * @param time
     */
    public Comment(int id, int bugId, String text, String author, Date time) {
        this.id = id;
        this.text = text;
        this.author = author;
        this.time = time;
        this.bugId = bugId;
    }

    /**
     * Creates a new {@link Comment} to be submitted to a Bugzilla installation
     *
     * @param text The text content of this comment
     * @param author
     */
    public Comment(int bugId, String text, String author, BugInfo bug) {
        this.id = -1;
        this.text = text;
        this.author = author;
        this.time = new Date(); 
        this.bugId = bugId;

    }

    /**
     * @return The unique ID of this {@link Comment}
     */

    public int getID() {
        return id;
    }

    /**
     * @return The text content of this {@link Comment}
     */

    public String getText() {
        return text;
    }

    public int getId() {
        return id;
    }
    
 
    public String getAuthor() {
        return author;
    }
    

    public Date getTime() {
        return time;
    }

    public int getBugId() {
        return bugId;
    }


}
