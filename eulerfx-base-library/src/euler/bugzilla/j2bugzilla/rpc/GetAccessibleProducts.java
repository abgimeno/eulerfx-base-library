/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.rpc;


import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author agime
 */
public class GetAccessibleProducts implements BugzillaMethod{
    
         private static final String METHOD_NAME = "Product.get_accessible_products";
       
        private List<Integer> ids = new ArrayList<>();
       
        /**
         * Returns an array of integers representing the accessible product IDs. These IDs can be further queried with the
         * {@link GetProduct} class.
         * @return An array of {@literal ints}.
         */
        public List<Integer> getProductIDs() {
                return ids;
        }
       
        @Override
        public void setResultMap(Map<Object, Object> hash) {
                Object[] objs = (Object[]) hash.get("ids");
                for(int i = 0; i < objs.length; i++) {
                        ids.add((Integer)objs[i]);
                }
        }

        @Override
        public Map<Object, Object> getParameterMap() {
                return Collections.emptyMap();
        }

        @Override
        public String getMethodName() {
                return METHOD_NAME;
        }

   
}
