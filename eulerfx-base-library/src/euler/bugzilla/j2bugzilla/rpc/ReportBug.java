/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.j2bugzilla.rpc;


import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


import euler.bugzilla.model.BugInfo;
import javafx.beans.property.Property;


/**
 * This class allows clients to report a new {@link Bug} to a Bugzilla installation.
 * @author Tom
 *
 */
public class ReportBug implements BugzillaMethod {

        /**
         * The method name Bugzilla will execute via XML-RPC
         */
        private static final String METHOD_NAME = "Bug.create";
       
        private Map<Object, Object> params = new HashMap<>();
        private Map<Object, Object> hash = new HashMap<>();
       
        /**
         * Creates a new {@link ReportBug} object to add a newly created {@link Bug}
         * to the appropriate Bugzilla installation
         *
         * @param bug A new {@link Bug} that should be tracked
         */
        public ReportBug(BugInfo bug) {
                Map<String, Property> internals = bug.getInternalState();
                params = new HashMap<>();
                for(String key : internals.keySet()) {
                        if(internals.get(key).equals("flags")) { continue; }//Flags are not supported as a creation parameter
                        if(internals.get(key).getValue() != null){
                            params.put(key, (Object)internals.get(key).getValue());
                        }
                }
        }
       
        public int getID() {
                if(hash.containsKey(BugInfo.ID)) {
                        Integer i = (Integer)hash.get(BugInfo.ID);
                        return i.intValue();
                } else {
                        return -1;
                }
        }


        @Override
        public void setResultMap(Map<Object, Object> hash) {
                this.hash = hash;
        }


        @Override
        public Map<Object, Object> getParameterMap() {
                return Collections.unmodifiableMap(params);
        }

 
        @Override
        public String getMethodName() {
                return METHOD_NAME;
        }
       
}

