/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.j2bugzilla.rpc;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import euler.bugzilla.j2bugzilla.base.Comment;
import euler.bugzilla.model.BugInfo;


/**
 * The <code>CommentBug</code> class allows clients to add a new comment to an
 * existing {@link Bug} in a Bugzilla installation.
 *
 * @author Tom
 *
 */
public class CommentBug implements BugzillaMethod {
       
        /**
         * The method Bugzilla will execute via XML-RPC
         */
        private static final String METHOD_NAME = "Bug.add_comment";
       
        private Map<Object, Object> params = new HashMap<>();
        private Map<Object, Object> hash = new HashMap<>();
       
        /**
         * Creates a new {@link CommentBug} object to add a comment to an
         * existing {@link Bug} in the client's installation
         *
         * @param bug A <code>Bug</code> to comment on
         * @param comment The comment to append
         */
        public CommentBug(BugInfo bug, String comment) {
                this(Integer.parseInt(bug.getId()), comment);
        }
       
        /**
         * Creates a new {@link CommentBug} object to add a {@link Comment}
         * to an existing {@link Bug} in the client's installation
         *
         * @param bug A {@literal Bug} to comment on
         * @param comment A {@literal Comment} to append
         */
        public CommentBug(BugInfo bug, Comment comment) {
                this(Integer.parseInt(bug.getId()), comment.getText());
        }
       
        /**
         * Creates a new {@link CommentBug} object to add a {@link Comment} to an existing
         * {@link Bug} in the client's installation
         * @param id The integer ID of an existing {@literal Bug}
         * @param comment A {@literal Comment} to append
         */
        public CommentBug(int id, Comment comment) {
                this(id, comment.getText());
        }
       
        /**
         * Creates a new {@link CommentBug} object to add a comment to an
         * existing {@link Bug} in the client's installation
         *
         * @param id The integer ID of an existing <code>Bug</code>
         * @param comment The comment to append
         */
        public CommentBug(int id, String comment) {
                params.put("id", id);
                params.put("comment", comment);
        }
       
       

        /**
         * Returns the ID of the newly appended comment
         * @return An <code>int</code> representing the ID of the new comment
         */
        public int getCommentID() {
                if(hash.containsKey("id")) {
                        Integer i = (Integer)hash.get("id");
                        return i.intValue();
                } else {
                        return -1;
                }
        }
       
        /**
         * {@inheritDoc}
         */
        @Override
        public void setResultMap(Map<Object, Object> hash) {
                this.hash = hash;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Map<Object, Object> getParameterMap() {
                return Collections.unmodifiableMap(params);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getMethodName() {
                return METHOD_NAME;
        }

}

