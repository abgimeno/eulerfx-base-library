/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.rpc;

import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import euler.bugzilla.j2bugzilla.base.Product;
import euler.bugzilla.j2bugzilla.base.ProductVersion;
import euler.bugzilla.j2bugzilla.enums.ProductParams;
import euler.bugzilla.model.query.SearchLimiter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * The {@literal GetProduct} class provides access to information on
 * {@link Product Products} active in a particular Bugzilla installation.
 */
public class GetProduct implements BugzillaMethod {

    /**
     * The method Bugzilla will execute via XML-RPC
     */
    private static final String METHOD_NAME = "Product.get";
    private Map<Object, Object> params = new HashMap<>();
    private Map<Object, Object> hash = Collections.emptyMap();
 	/**
	 * Creates a new {@link GetProduct}, which can be used to retrieve the {@link Product} associated with the
	 * specified ID number.
	 * @param id A unique integer ID.
	 */
	public GetProduct(int id) {
		params.put("ids", new Integer[] { id });
	}
    /**
     * Creates a new {@link GetProduct}, which can be used to retrieve the
     * {@link Product} associated with the specified ID number.
     *
     * @param id A unique integer ID.
     */
    public GetProduct(List<Integer> ids) {
        params.put("ids", ids);
    }

    public GetProduct(List<Integer> ids, Map<ProductParams, List<String>> params) {
        this(ids);
        for(ProductParams sl : params.keySet()){
            this.params.put(sl.getType(), params.get(sl).toArray());
        }
    }
    

    /**
     * Returns the product found in the Bugzilla installation matching the
     * provided name or ID.
     *
     * @return A new {@link Product}, or null if there are no results to return.
     */
    public List<Product> getProducts() {
        
        Object products = hash.get("products");
        Logger.getLogger(this.getClass()).debug("Products: "+ products);
        if (products == null) {
            return null;
        }

        Object[] arr = (Object[]) products;
        if (arr.length == 0) {
            return null;
        }
        
        List<Object> results = Arrays.asList(arr);
        return extractProducts(results);
        

    }

    @Override
    public void setResultMap(Map<Object, Object> hash) {
        Logger.getLogger(this.getClass()).debug("Setting result hash: "+ hash);
        this.hash = hash;
    }

    @Override
    public Map<Object, Object> getParameterMap() {
        Logger.getLogger(this.getClass()).debug("Getting parameters map: "+ params);
        return Collections.unmodifiableMap(params);
    }

    @Override
    public String getMethodName() {
        return METHOD_NAME;
    }

    private List<Product> extractProducts(List<Object> results) {
        Logger.getLogger(this.getClass()).debug("extractProducts started: "+ results);
        List<Product> items = new ArrayList<>();
        for (Object o : results) {
            Map<Object, Object> prodMap = (Map<Object, Object>) o;
            Product product = new Product((Integer) prodMap.get("id"), (String) prodMap.get("name"));
            String desc = (String) prodMap.get("description");
            product.setDescription(desc);

            if (prodMap.get("versions") != null) {
                Object[] versions = (Object[]) prodMap.get("versions");
                for (Object version : versions) {
                    @SuppressWarnings("unchecked")//Cast to form specified by webservice
                    Map<Object, Object> versionMap = (Map<Object, Object>) version;
                    /*ProductVersion productVersion = new ProductVersion((Integer) versionMap.get("id"),
                            (String) versionMap.get("name"));
                    product.addProductVersion(productVersion);*/
                }
            }

            items.add(product);
        }
        return items;    }
	/**
	 * Returns the product found in the Bugzilla installation matching the provided name or ID.
	 * @return A new {@link Product}, or null if there are no results to return.
	 */
	public Product getProduct() {
		Object products = hash.get("products");
		if(products == null) { return null; }
		
		Object[] arr = (Object[])products;
		if(arr.length == 0) { return null; }
		
		@SuppressWarnings("unchecked")//Cast to form specified by webservice
		Map<Object, Object> prodMap = (Map<Object, Object>)arr[0];
		Product product = new Product((Integer)prodMap.get("id"), (String)prodMap.get("name"));
		String desc = (String)prodMap.get("description");
		product.setDescription(desc);
		
		if(prodMap.get("versions") != null) {
			Object[] versions = (Object[]) prodMap.get("versions");
			for(Object version : versions){
				@SuppressWarnings("unchecked")//Cast to form specified by webservice
				Map<Object, Object> versionMap = (Map<Object, Object>) version;
				ProductVersion productVersion = new ProductVersion((Integer)versionMap.get("id"), 
						(String)versionMap.get("name"));
				product.addProductVersion(productVersion);
			}
		}
		
		return product;
	}    
}
