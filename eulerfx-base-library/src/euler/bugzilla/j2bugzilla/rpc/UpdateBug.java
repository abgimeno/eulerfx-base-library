/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.j2bugzilla.rpc;

import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import euler.bugzilla.model.BugInfo;

/**
 * The {@literal UpdateBug} class allows clients to update an existing {@link Bug} on the installation with
 * new values. Currently, this method only allows one bug at a time to be updated.
 *
 * Note that Bugzilla 3.6 does not allow updating bugs via the webservice.
 * @author Tom
 *
 */
public class UpdateBug implements BugzillaMethod {
       
        /**
         * The method name for this webservice operation.
         */
        private static final String METHOD_NAME = "Bug.update";

        /**
         * A {@link Bug} to update on the installation.
         */
        private final BugInfo bug;
       
        /**
         * Creates a new {@link UpdateBug} object to submit to the Bugzilla webservice. The {@link Bug} on the
         * installation identified by the id or alias of the bug provided will have its fields updated
         * to match those of the values in the provided bug.
         * @param bug
         */
        public UpdateBug(BugInfo bug) {
                this.bug = bug;
        }
       
        /**
         * {@inheritDoc}
        * @param hash
         */
        @Override
        public void setResultMap(Map<Object, Object> hash) {
                Object[] modified = (Object[])hash.get("bugs");
                //For now, we only modify one bug at a time, thus this array should be a single element
                assert(modified.length == 1);
                //There aren't a ton of useful elements returned, so for now just discard the map.      
        }

        /**
         * {@inheritDoc}
        * @return 
         */
        @Override
        public Map<Object, Object> getParameterMap() {
                Map<Object, Object> params = new HashMap<>();
               
                params.put("ids", bug.getId());
               
                copyNotNull(params, BugInfo.ALIAS, bug.getAlias());
                copyNotNull(params, BugInfo.SUMMARY, bug.getSummary());
                copyNotNull(params, BugInfo.PRIORITY, bug.getPriority());
                copyNotNull(params, BugInfo.PRODUCT, bug.getProduct());
                copyNotNull(params, BugInfo.COMPONENT, bug.getComponent());
                copyNotNull(params, BugInfo.VERSION, bug.getVersion());
                copyNotNull(params, BugInfo.STATUS, bug.getStatus());
                copyNotNull(params, BugInfo.RESOLUTION, bug.getResolution());
                copyNotNull(params, BugInfo.OS, bug.getOpsys());
                copyNotNull(params, BugInfo.PLATFORM, bug.getPlatform());
                copyNotNull(params, BugInfo.SEVERITY, bug.getSeverity());
               
                return Collections.unmodifiableMap(params);
        }
       
        private void copyNotNull(Map<Object, Object> map, String key, Object value) {
                if(value != null) {
                        map.put(key, value);
                }
        }
       
        /**
         * {@inheritDoc}
        * @return 
         */
        @Override
        public String getMethodName() {
                return METHOD_NAME;
        }
}
