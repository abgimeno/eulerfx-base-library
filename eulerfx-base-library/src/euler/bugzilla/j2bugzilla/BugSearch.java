package euler.bugzilla.j2bugzilla;

/*
updated from j2bugzilla
 */
import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import euler.bugzilla.model.Bug;
import euler.bugzilla.model.query.QueryParameters;
import euler.bugzilla.model.query.SearchLimiter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides convenience methods for searching for {@link Bug Bugs} on
 * your installation.
 *
 * Clients should note that Bugzilla does not seem to respond well when
 * returning large numbers of search results via the webservice interface
 * (around 2000 in some tests caused it to fail with an HTTP 500 error). Try and
 * keep searches as focused as possible for best results.
 *
originial j2bugzilla file has been modified
 */
public class BugSearch implements BugzillaMethod {

    /**
     * The method Bugzilla will execute via XML-RPC
     */
    private static final String METHOD_NAME = "Bug.search";
    
    /**
     * A {@literal Map} returned by the XML-RPC method.
     */
    private Map<Object, Object> hash = new HashMap<>();
    
    /**
     * A {@literal Map} used by the XML-RPC method containing the required object
     * parameters.
     */
    private final Map<Object, Object> params = new HashMap<>();
       

    /**
     * Creates a new {@link BugSearch} object to query the Bugzilla installation
     * based on one or more {@link SearchQuery SearchQueries}.
     *
     * @param queries One or more {@literal SearchQuery} objects to narrow the
     * search by.
     */
    public BugSearch(QueryParameters[] queries) {
        if (queries.length == 0) {
            throw new IllegalArgumentException("At least one search query is required");
        }
        for (QueryParameters query : queries) {
            params.put(query.getLimiter().getName(), query.getQuery());
        }
    }

    /**
     * Returns the {@link Bug Bugs} found by the query as a
     * <code>List</code>
     *
     * @return a {@link List} of {@link Bug Bugs} that match the query and limit
     */
    public List<Bug> getSearchResults() {

        List<Bug> results = new ArrayList<>();
        /*
         * The following is messy, but necessary due to how the returned XML document nests
         * Maps.
         */
        if (hash.containsKey("bugs")) {
            Object[] bugs = (Object[]) hash.get("bugs");
            for (Object o : bugs) {
                @SuppressWarnings("unchecked")
                Map<String, Object> bugMap = (HashMap<String, Object>) o;
                /*Handle version property for older Bugzillas which did not include it in the public portion of the hash
                if (!bugMap.containsKey("version")) {
                    Map<?, ?> internals = (Map<?, ?>) bugMap.get("internals");
                    bugMap.put("version", internals.get("version"));
                }*/
                Bug bug = new BugFactory().createBug(bugMap);
                results.add(bug);
            }
        }
        return results;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setResultMap(Map<Object, Object> hash) {
        this.hash = hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Object, Object> getParameterMap() {
        return params;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMethodName() {
        return METHOD_NAME;
    }

    
}
