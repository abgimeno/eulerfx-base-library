/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.history;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author agime
 * Here we set what change we expect to find within BugHistoryInfo
 * Max values of ocurrences are 2 ("value1" and "value2"). Why only 2 and not a list, as
 * proper programming would suggest? Well, this parameter is compared against
 * BugHistoryChange and it uses a OR in order to avoid another list iteration, as they're
 * already many iterations due to the data structures used to return the bug history. 
 * It can be an enhancement to think about though
 *
 * the boolean flag returns whether one value or two values were set
 * 
 * The SearchDirection defines if we expect the change to have occurred before or after the given date
 */
public class BugHistoryParameter {
   
    private List<String> values = new ArrayList<>();
    private Date date;
    private Date date2;
    private BugHistoryInfo.SearchDirection dir;
    private boolean has2Values=false;
    private boolean has2Dates=false;

    public BugHistoryParameter(List<String> values, BugHistoryInfo.SearchDirection dir, Date date) {
        this.values = values;
        this.date = date;
        this.dir = dir;
        this.has2Values=true;
    }

    public BugHistoryParameter(List<String> values, Date from, Date to) {
        this.values=values;
        this.date = from;
        this.date2= to;
        this.dir = BugHistoryInfo.SearchDirection.BETWEEN;
        this.has2Values=false;
        this.has2Dates=true;        
    }  
    
    
    public String getValue1() {
        return this.values.get(1);
    }

    public void setValue1(String value1) {
        this.values.set(1, value1);
    }
    public String getValue2() {
        return this.values.get(2);
    }
    public void setValue2(String value2) {
        this.values.set(2, value2);
    }
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BugHistoryInfo.SearchDirection getDir() {
        return dir;
    }

    public void setDir(BugHistoryInfo.SearchDirection dir) {
        this.dir = dir;
    }

    public boolean isHas2Values() {
        return has2Values;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public boolean isHas2Dates() {
        return has2Dates;
    }

    public void setHas2Dates(boolean has2Dates) {
        this.has2Dates = has2Dates;
    }
    public Date getTo() {
        return date2;
    }
    public Date getFrom() {
        return date;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
    
}
