package euler.bugzilla.j2bugzilla.history;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import euler.bugzilla.j2bugzilla.base.BugzillaMethod;
import euler.bugzilla.model.Bug;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides convenience methods for searching for {@link Bug Bugs} on
 * your installation.
 *
 * Clients should note that Bugzilla does not seem to respond well when
 * returning large numbers of search results via the webservice interface
 * (around 2000 in some tests caused it to fail with an HTTP 500 error). Try and
 * keep searches as focused as possible for best results.
 *
 * @author Tom
 *
 */
public class BugHistory implements BugzillaMethod {

    /**
     * The method Bugzilla will execute via XML-RPC
     */
    private static final String METHOD_NAME = "Bug.history";
    
    /**
     * A {@code Map} returned by the XML-RPC method.
     */
    private Map<Object, Object> hash = new HashMap<>();
    
    /**
     * A {@code Map} used by the XML-RPC method containing the required object
     * parameters.
     */
    private final Map<Object, Object> params = new HashMap<Object, Object>();



    /**
     * Creates a new {@link BugSearch} object to query the Bugzilla installation
     * based on one or more {@link SearchQuery SearchQueries}.
     *
     * @param queries One or more {@code SearchQuery} objects to narrow the
     * search by.
     */
    public BugHistory(Object[] parameters) {
        if (parameters.length == 0) {
            throw new IllegalArgumentException("At least one search query is required");
        }
        params.put("ids", parameters);

    }

    /**
     * Returns the {@link Bug Bugs} found by the query as a
     * <code>List</code>
     *
     * @return a {@link List} of {@link Bug Bugs} that match the query and limit
     */
    public Map<Integer, BugHistoryInfo> getBugsHistory() {

        Map<Integer, BugHistoryInfo> results = new HashMap<>();
        /*
         * The following is messy, but necessary due to how the returned XML document nests
         * Maps.
         */
        if (hash.containsKey("bugs")) {
            Object[] bugs = (Object[]) hash.get("bugs");
            for (Object o : bugs) {
                @SuppressWarnings("unchecked")
                Map<String, Object> historyMap = (HashMap<String, Object>) o;
                /*Handle version property for older Bugzillas which did not include it in the public portion of the hash
                if (!bugMap.containsKey("version")) {
                    Map<?, ?> internals = (Map<?, ?>) bugMap.get("internals");
                    bugMap.put("version", internals.get("version"));
                }*/
                //Bug bug = new MyBugFactory().createBug(bugMap);
                //Logger.getLogger(this.getClass())"");
                Integer id =(Integer) historyMap.get("id");
                results.put(id, new BugHistoryInfo(historyMap));
            }
        }
        return results;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setResultMap(Map<Object, Object> hash) {
        this.hash = hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Object, Object> getParameterMap() {
        return params;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMethodName() {
        return METHOD_NAME;
    }

}
