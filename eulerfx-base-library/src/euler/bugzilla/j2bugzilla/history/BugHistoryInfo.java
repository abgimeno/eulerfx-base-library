/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.history;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author agime
 * Class to store bug history items, this can be 
 * made serializable, annotating with JPA
 */
public class BugHistoryInfo {
    
    private Integer id;  
    private List<BugHistoryChange> changes = new ArrayList<>();
    
    public BugHistoryInfo(Integer id, Object[] changeHistory) {       

    }

    public BugHistoryInfo(Map<String, Object> historyMap) {     
        this.id=(Integer) historyMap.get("id");        
        historyMap.remove("id");
        Object[] bugChanges = (Object[]) historyMap.get("history"); 
        for(Object o: bugChanges){
            Map<String, Object> map = (HashMap) o;
                changes.add(new BugHistoryChange(map));                      
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<BugHistoryChange> getChanges() {
        return changes;
    }

    public void setChanges(List<BugHistoryChange> changes) {
        this.changes = changes;
    }    
    
    public boolean searchOnAddedField(BugHistoryParameter bhp){
        if(bhp.isHas2Dates() && bhp.isHas2Values()){
            return searchOnAddedField(bhp.getValues(), bhp.getFrom(), bhp.getTo());
        }
        if(bhp.isHas2Dates() && !bhp.isHas2Values()){
            return searchOnAddedField(bhp.getValues(), bhp.getFrom(), bhp.getTo());
        }
        return (bhp.isHas2Values()) ? searchOnAddedField(bhp.getValues(), bhp.getDir(), bhp.getDate())
                :searchOnAddedField(bhp.getValues(),bhp.getDir(),bhp.getDate());
    }    
    
    private boolean searchOnAddedField(List<String> values, BugHistoryInfo.SearchDirection dir, Date date){
        for (BugHistoryChange bhc : changes){
            if(bhc.added(values, dir, date)){
                return true;
            }
        }
        return false;
    }    
    
    private boolean searchOnAddedField(List<String> values, Date from, Date to){
        for (BugHistoryChange bhc : changes){
            if(bhc.added(values, from, to)){
                return true;
            }
        }
        return false;
    }
    
    public enum SearchDirection {
        /*
         *if while searching using date we should return true
         * if item has been changed after or before (backwards) the date provided
         */
        BEFORE(1),

        AFTER(2),
        
        BETWEEN(3);
               
        private final int direction;

        /**
         * Creates a new {@link SearchLimiter} with the designated name
         *
         * @param name The name Bugzilla expects for this search limiter
         */
        SearchDirection(int direction) {
            this.direction = direction;
        }

        /**
         * Get the name Bugzilla expects for this search limiter
         *
         * @return A <code>String</code> representing the search limiter
         */
        int getDirection() {
            return this.direction;
        }
    }    
}
