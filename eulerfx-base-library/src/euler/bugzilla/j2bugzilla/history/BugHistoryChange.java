/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.history;

import static euler.bugzilla.j2bugzilla.history.BugHistoryInfo.SearchDirection.BEFORE;
import static euler.bugzilla.j2bugzilla.history.BugHistoryInfo.SearchDirection.AFTER;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author agime
 */
public class BugHistoryChange {

    /*
     * The web service returns a map (which is passed here to the constructor) that contains:
     * a Date (date of the change)
     * a String who (who performed the change)
     * and a Map of changes. 
     */
    private Date when;
    private String who;
    private List<Map<String, String>> changes = new ArrayList<>();
    /*
     * The in the changes map all history of changes are stored, 
     * it contains 3 keys, which are declared as static final below
     */
    private static final String ADDED = "added";
    private static final String REMOVED = "removed";
    private static final String FIELD = "field_name";

    public BugHistoryChange(Map list) {
        this.when = (Date) list.get("when");
        this.who = (String) list.get("who");
        Object[] bugChanges = (Object[]) list.get("changes");
        for (Object o : bugChanges) {
            changes.add((HashMap) o);
        }
    }

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public List<Map<String, String>> getChanges() {
        return changes;
    }

    public void setChanges(List<Map<String, String>> changes) {
        this.changes = changes;
    }
    /*
     * the below is a bit messy and code is repeated so probably 
     * code can be cleaned using functions and parameters. it's not straightforward 
     * as it would appear, so leaving like this to refractor in future
     *
     * basically iterates the changehistory searching for a given change
     * it also looks if change was performed before or after a date, depedining on
     * parameter passed
     * 
     */

    public boolean added(List<String> changes, BugHistoryInfo.SearchDirection dir, Date date) {
        switch (dir) {
            case AFTER:
                return addedAfter(changes, date);
            case BEFORE:
                return addedBefore(changes, date);
        }
        return false;
    }
    private boolean addedAfter(List<String> values, Date date) {
        if (when.after(date)) {
            for (Map<String, String> map : changes) {
                String keyContent = map.get(ADDED);
                for (String change : values) {
                    if (keyContent.equals(change)) {
                        //Logger.getLogger(this.getClass())"I've found TRUE that " + keyContent + " is equals " + change);
                        //Logger.getLogger(this.getClass())"This change was made on " + df.format(when));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean addedBefore(List<String> values, Date date) {
        if (when.before(date)) {
            for (Map<String, String> map : changes) {
                String keyContent = map.get(ADDED);
                for (String change : values) {
                    if (keyContent.equals(change)) {
                        //Logger.getLogger(this.getClass())"I've found TRUE that " + keyContent + " is equals " + change);
                        //Logger.getLogger(this.getClass())"This change was made on " + df.format(when));
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public boolean added(List<String> values, Date from, Date to) {
       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean removed(List<String> values, BugHistoryInfo.SearchDirection dir, Date date) {
        switch (dir) {
            case AFTER:
                return removedAfter(values, date);
            case BEFORE:
                return removedBefore(values, date);
        }
        return false;
    }

    public boolean removed(List<String> values, Date from, Date to) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }
    //TO-DO: remove within dates is missing

    private boolean removedAfter(List<String> values, Date date) {
        if (when.after(date)) {
            for (Map<String, String> map : changes) {
                String keyContent = map.get(REMOVED);
                for (String change : values) {
                    if (keyContent.equals(change)) {
                        //Logger.getLogger(this.getClass())"I've found TRUE that " + keyContent + " is equals " + change);
                        //Logger.getLogger(this.getClass())"This change was made on " + df.format(when));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean removedBefore(List<String> values, Date date) {
        if (when.before(date)) {
            for (Map<String, String> map : changes) {
                String keyContent = map.get(REMOVED);
                for (String change : values) {
                    if (keyContent.equals(change)) {
                        //Logger.getLogger(this.getClass())"I've found TRUE that " + keyContent + " is equals " + change);
                        //Logger.getLogger(this.getClass())"This change was made on " + df.format(when));
                        return true;
                    }
                }
            }
        }
        return false;
    }


    

}
