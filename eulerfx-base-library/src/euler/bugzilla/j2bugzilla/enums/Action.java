/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.j2bugzilla.enums;

/**
 *
 * @author agime
 */
public enum Action {
    
    CREATE(1), 
    MODIFY(2),
    LOAD(3);

    private final int type;

    /**
     * Creates a new {@link UserInfoParams} with the designated name
     *
     * @param name The name Bugzilla expects for this search limiter
     */
    Action(int type) {
        this.type = type;
    }

    /**
     * Get the name Bugzilla expects for this UserInfoParams
     *
     * @return A <code>String</code> representing the search limiter
     */
    public int getType() {
        return this.type;
    }    
}
