package euler.bugzilla.j2bugzilla.enums;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author agime
 */
public enum BugzillaUserParams {

    ID("id"), 
    ENABLED("can_login"), // Boolean
    NAME("name"),
    EMAIL("email"), 
    REAL_NAME("real_name"), 
    GROUPS("groups");//Array;   

    private final String type;

    /**
     * Creates a new {@link UserInfoParams} with the designated name
     *
     * @param name The name Bugzilla expects for this search limiter
     */
    BugzillaUserParams(String type) {
        this.type = type;
    }

    /**
     * Get the name Bugzilla expects for this UserInfoParams
     *
     * @return A <code>String</code> representing the search limiter
     */
    public String getType() {
        return this.type;
    }
}
