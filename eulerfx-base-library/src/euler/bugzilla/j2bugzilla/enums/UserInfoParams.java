/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.j2bugzilla.enums;

/**
 *
 * @author agime
 */
public enum UserInfoParams {

    IDS("ids"), /*array An array of integers, representing user ids.  
     * Logged-out users cannot pass this parameter to this function. If they try, they will get an error. 
     * Logged-in users will get an error if they specify the id of a user they cannot see.*/
    NAMES("names"),// array An array of login names (strings).
    INCLUDE("include_fields"),
    EXCLUDE("exclude_fields"),
    MATCH("match");/* (array)  An array of strings. This works just like "user matching" in Bugzilla itself. Users will be returned whose real name or login name contains any one of the specified strings. Users that you cannot see will not be included in the returned list.
     Some Bugzilla installations have user-matching turned off, in which case you will only be returned exact matches.
     Most installations have a limit on how many matches are returned for each string, which defaults to 1000 but can be changed by the Bugzilla administrator.
     Logged-out users cannot use this argument, and an error will be thrown if they try. (This is to make it harder for spammers to harvest email addresses from Bugzilla, and also to enforce the user visibility restrictions that are implemented on some Bugzillas.)*/    

    private final String type;

    /**
     * Creates a new {@link UserInfoParams} with the designated name
     *
     * @param name The name Bugzilla expects for this search limiter
     */
    UserInfoParams(String type) {
        this.type = type;
    }

    /**
     * Get the name Bugzilla expects for this UserInfoParams
     *
     * @return A <code>String</code> representing the search limiter
     */
    public String getType() {
        return this.type;
    }
}
