/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.fx.table;

import euler.bugzilla.exception.TableColumnNotFoundException;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.utils.EulerExceptionUtils;
import euler.fx8.collections.BugPredicate;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.EventType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 *
 * @author agime
 */
public class BugTableView extends TableView {

    private final FilterPane filter = new FilterPane();
    private FilteredList<BugInfo> filteredList = null;
    private final ObservableList source = FXCollections.observableArrayList(); // we store here the original list

    public BugTableView() {
        super();
        try {
            getColumns().addListener(new ListChangeListener<TableColumn>() {
                // when I add a new column the name of the column is added as fields to filter for
                @Override
                public void onChanged(ListChangeListener.Change<? extends TableColumn> c) {
                    while (c.next()) {
                        if (c.wasPermutated()) {
                            for (int i = c.getFrom(); i < c.getTo(); ++i) {
                                //permutate actions
                            }
                        } else if (c.wasUpdated()) {
                            //update item
                        } else {
                            for (TableColumn remitem : c.getRemoved()) {
                                filter.removeFilter(remitem.getId());
                            }
                            for (TableColumn additem : c.getAddedSubList()) {
                                filter.addFilter(additem.getId());
                            }
                        }
                    }
                }
            });
            filter.getFilterField().textProperty().addListener(new ChangeListener<String>() {

                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, final String newValue) {
                    if (newValue.isEmpty()) { // there is no text
                        setItems(source);
                    } else {
                        
                        BugPredicate predicate = new BugPredicate((String) filter.getFieldChoice().valueProperty().getValue(), newValue);
                        if (filteredList == null) {
                            source.addAll(getItems()); //here I save the original list 
                        }
                        if (oldValue.length() > newValue.length()) { // if user deletes a character I filter the original list
                            filteredList = new FilteredList(FXCollections.observableArrayList(source), predicate);
                        } else {
                            filteredList = new FilteredList(FXCollections.observableArrayList(getItems()), predicate);
                        }
                        setItems(filteredList);
                    }
                    fireChange();
                }
            });
        } catch (Exception ex) {
            EulerExceptionUtils.getInstance().handleException(this.getClass(), ex);
        }
    }

    protected final void fireChange() {
        TableColumn tc = (TableColumn) getColumns().get(0);
        tc.setVisible(false);
        tc.setVisible(true);
    }

    public TableColumn getColumn(String columnId) throws TableColumnNotFoundException {
        for (Object obj : getColumns()) {
            TableColumn tc = (TableColumn) obj;
            if (tc.getId().equals(columnId)) {
                return tc;
            }
        }
        StringBuilder sb = new StringBuilder("Column with id: ");
        sb.append(columnId);
        sb.append(" cannot be found on table");
        throw new TableColumnNotFoundException(sb.toString());
    }

    public FilterPane getFilter() {
        return filter;
    }

}
