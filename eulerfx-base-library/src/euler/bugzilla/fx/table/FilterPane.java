/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.fx.table;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author agime
 */
public class FilterPane extends AnchorPane{
    
    private final HBox container = new HBox();
    private final TextField filterField = new TextField();
    private final ChoiceBox fieldChoice = new ChoiceBox();
    private static final double PREF_HEIGHT=32.0;
    private static final double PREF_WIDTH=304.0;
            
    public FilterPane() {
        initialiseFilterfields();
    }
    
    private void initialiseFilterfields(){

        this.getChildren().add(container);
        setPrefHeight(PREF_HEIGHT);
        setPrefWidth(PREF_WIDTH);
        container.setPrefHeight(PREF_HEIGHT);
        container.setPrefWidth(PREF_WIDTH);        
        filterField.setPrefWidth(200.0);        
        filterField.setPrefHeight(25);
        filterField.setPromptText("Filter these bugs ....");
        fieldChoice.setPrefHeight(25);
        fieldChoice.setPrefWidth(200.0);        
        container.getChildren().add(filterField);
        container.getChildren().add(fieldChoice);
    }

    public void addFilter(String filterName){        
        fieldChoice.getItems().add(filterName);
        if(filterName.equals("summary")){ // selects summary as default filter
            fieldChoice.getSelectionModel().select(fieldChoice.getItems().indexOf("summary"));        
        }
    }
    public void removeFilter(String filterName){        
        fieldChoice.getItems().remove(filterName);
    } 

    public TextField getFilterField() {
        return filterField;
    }

    public ChoiceBox getFieldChoice() {
        return fieldChoice;
    }
    
}
