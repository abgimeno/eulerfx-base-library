/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans;

import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.query.Query;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class BugInfoBean extends AbstractBean {

    public BugInfoBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }

    public BugInfo find(Integer id) {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            BugInfo stat = (BugInfo) em.find(BugInfo.class, id);
            em.getTransaction().commit();
            em.close();
            return stat;
        } catch (NoResultException ex) {
            Logger.getLogger(this.getClass()).error("No result returned for id " + id);
            Logger.getLogger(this.getClass()).error(null,ex);
            return null;
        }
    }

    public List<BugInfo> findQueryBugs(String queryName) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<BugInfo> bugs = (List<BugInfo>) em.createNamedQuery("Query.fetchBugsbyName").setParameter("queryName", queryName).getResultList();
        em.getTransaction().commit();
        em.close();
        return bugs;
    }
    public List<BugInfo> findQueryBugs(Query query) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<BugInfo> bugs = (List<BugInfo>) em.createNamedQuery("Query.fetchBugs").setParameter("query", query).getResultList();
        em.getTransaction().commit();
        em.close();
        return bugs;
    }    
}
