/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans;

import euler.bugzilla.j2bugzilla.base.Product;
import euler.bugzilla.model.ProductInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class ProductBean extends AbstractBean<ProductInfo> {

    public ProductBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }

    public ProductInfo find(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        ProductInfo pi = (ProductInfo) em.find(ProductInfo.class, id);
        em.getTransaction().commit();
        em.close();
        return pi;

    }

    public Long getNProducts() {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            Long nProds = (Long) em.createNamedQuery("PI.countProducts").getSingleResult();
            em.getTransaction().commit();
            em.close();
            return nProds;
        } catch (NoResultException ex) {
            Logger.getLogger(this.getClass()).error(null,ex);
            return new Long(0);
        }
    }

    public ObservableList<ProductInfo> getAllProds() {
        ObservableList<ProductInfo> items = new SimpleListProperty(FXCollections.<ProductInfo>observableArrayList());
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<ProductInfo> products = em.createNamedQuery("PI.getAll").getResultList();
        em.getTransaction().commit();
        em.close();
        items.addAll(products);
        return items;
    }

    public ObservableList<ProductInfo> getActiveProds() {
        ObservableList<ProductInfo> items = new SimpleListProperty(FXCollections.<ProductInfo>observableArrayList());
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<ProductInfo> products = em.createNamedQuery("PI.getActive").getResultList();
        em.getTransaction().commit();
        em.close();
        items.addAll(products);
        return items;
    }    
    public List<Product> getProductList() {
        ObservableList<ProductInfo> items = getAllProds();
        List<Product> products = new ArrayList();
        for(ProductInfo pi: items){
            Product p = new Product(pi.getId(), pi.getName());
            products.add(p);
        }
        return products;
    }    
    
    public List<Product> getActiveProductList() {
        ObservableList<ProductInfo> items = getActiveProds();
        List<Product> products = new ArrayList<>();
        for (ProductInfo pi : items) {            
            Product p = new Product(pi.getId(), pi.getName());
            products.add(p);
        }
        return products;
    }  

    public ProductInfo getProduct(String name) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        ProductInfo product = (ProductInfo) em.createNamedQuery("PI.getByName")
                                        .setParameter("name",name)
                                        .getSingleResult();
        em.getTransaction().commit();
        em.close();
        return product;        
    }
    
    public List<ProductInfo> getProductList(List<String> names) {
        List<ProductInfo> products = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        for (String name : names) {
            ProductInfo product = (ProductInfo) em.createNamedQuery("PI.getByName")
                    .setParameter("name", name)
                    .getSingleResult();
            products.add(product);
        }
        em.getTransaction().commit();
        em.close();
        return products;
    }
}
