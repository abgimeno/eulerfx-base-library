/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans.query;

import euler.bugzilla.beans.AbstractBean;
import euler.bugzilla.model.ProductInfo;
import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.LegalValue;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;

/**
 *
 * @author root
 */
public class LegalValueBean extends AbstractBean<LegalValue> {

    public LegalValueBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }
    
    public List getLegalValues(Fields field) {      
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List values =  em.createNamedQuery("LV.fetchLegalValues")
                                    .setParameter("field",field)
                                    .getResultList();
        em.getTransaction().commit();
        em.close();   
        Logger.getLogger(this.getClass()).debug(values);
        return values;
    }
    public List getLegalValues(Fields field, ProductInfo product) {      
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List values =  em.createNamedQuery("LV.fetchLegalValues4Product")
                                    .setParameter("field",field)
                                    .setParameter("product",product)
                                    .getResultList();
        em.getTransaction().commit();
        em.close();   
        Logger.getLogger(this.getClass()).debug(values);
        return values;
    }    
    public List getLegalValues(Fields field, List<ProductInfo> products) {      
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List values =  em.createNamedQuery("LV.fetchLegalValuesInProducts")
                                    .setParameter("field",field)
                                    .setParameter("products",products)
                                    .getResultList();
        em.getTransaction().commit();
        em.close();   
        Logger.getLogger(this.getClass()).debug(values);
        return values;
    }    
        
}
