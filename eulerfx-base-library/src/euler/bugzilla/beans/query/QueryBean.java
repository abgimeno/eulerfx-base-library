/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans.query;

import euler.bugzilla.model.query.Query;
import euler.bugzilla.model.query.QueryFolder;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import euler.bugzilla.beans.AbstractBean;
import euler.fx.tree.TreeElement;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.ServerInformation;
import euler.bugzilla.model.conf.QueryTableConf;
import euler.bugzilla.model.query.TreeTypeEnum;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;

/**
 *
 * @author root
 */
public class QueryBean extends AbstractBean<TreeElement> {

    public QueryBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }

    public List<Query> getAllQueries() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<Query> queries = (List<Query>) em.createNamedQuery("Query.getAll").getResultList();
        em.getTransaction().commit();
        em.close();
        return queries;
    }

    public List<Query> getQueries(QueryFolder folder) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<Query> queries = (List<Query>) em.createNamedQuery("Query.byFolder")
                .setParameter("folder", folder)
                .getResultList();
        em.getTransaction().commit();
        em.close();
        return queries;
    }

    public ServerInformation getServer(Query q) {
        q = find(q.getId());
        return q.getServer();
    }        
    public List<QueryFolder> getAllQueryFolders() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<QueryFolder> folders = (List<QueryFolder>) em.createNamedQuery("QF.getAllFetchQueries").getResultList();
        em.getTransaction().commit();
        em.close();
        return folders;
    }

    public Query getQuery(String name) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Query query = (Query) em.createNamedQuery("Query.byName").setParameter("queryName", name).getSingleResult();
        em.getTransaction().commit();
        em.close();
        return query;
    }

    public TreeElement find(Long id, TreeTypeEnum type) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        TreeElement query = (TreeElement) em.find(type.getType(), id);
        em.getTransaction().commit();
        em.close();
        return query;
    }

    public QueryFolder findFolder(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        QueryFolder queryFolder = (QueryFolder) em.find(QueryFolder.class, id);
        em.getTransaction().commit();
        em.close();
        return queryFolder;
    }
    public QueryFolder findFolderFetchQueries(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        QueryFolder queryFolder = (QueryFolder) em.createNamedQuery("QF.fetchQueries").setParameter("id", id).getSingleResult();
        em.getTransaction().commit();
        em.close();
        return queryFolder;
    }
    public Query find(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Query query = (Query) em.find(Query.class, id);
        em.getTransaction().commit();
        em.close();
        return query;
    }
    public Long countBugs(Long id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Long results = (Long) em.createNamedQuery("Query.countBugs").setParameter("queryId",id).getSingleResult();
        em.getTransaction().commit();
        em.close();
        return results;
    }    

    public List<BugInfo> persistBugs(List results) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<BugInfo> bugs = results;
        for (BugInfo bi : bugs) {            
            try {
                em.find(BugInfo.class,bi.getBugId());
                em.merge(bi);
            } catch (NoResultException exc) {
                em.persist(bi);
                Logger.getLogger(this.getClass()).error(bi.getBugId() + " cannot be found. Persisting it on DB.");
            }
        }
        em.getTransaction().commit();
        em.close();
        return bugs;
    }

    
    /*Removal of queries deals also with removing associated data 
    this action is performed within the same transaction to allow rolling back
    other instances of TreeElement call super()
    */
    @Override
    public void remove(TreeElement item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        if(item instanceof Query){
            // retrieve managed instance of Query from DB
            Query sourceQuery = (Query) em.find(Query.class, item.getId());
            //find associated data items
            Query dbQuery = em.find(Query.class, sourceQuery.getId());
            try{
                QueryTableConf conf = (QueryTableConf) em.createNamedQuery("QueryTableConf.getConfiguration")
                .setParameter("query", dbQuery).getSingleResult();               
                // proceed with removal of associated configuration
                em.remove(conf);
            }catch(NoResultException ex){
                Logger.getLogger(this.getClass()).info("No configuration for query "+sourceQuery.getDisplayName() +" was found");
            }            
            em.remove(sourceQuery);
            em.getTransaction().commit();
            em.close();
        }
        else{
            super.remove(item);
        } //To change body of generated methods, choose Tools | Templates.
    }
  
}
