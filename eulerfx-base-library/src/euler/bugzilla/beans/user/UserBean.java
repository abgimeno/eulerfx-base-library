/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.beans.user;

import euler.bugzilla.beans.AbstractBean;
import euler.bugzilla.model.BugzillaUser;
import euler.bugzilla.model.ServerInformation;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class UserBean extends AbstractBean<BugzillaUser>{

    public UserBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }
    public List<BugzillaUser> getAllUsers(ServerInformation bc){
        Logger.getLogger(this.getClass()).debug("Server information is passed as parameter but not used yet");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List<BugzillaUser> users = (List<BugzillaUser>) em.createNamedQuery("BUsers.getUsers")                
                .getResultList();
        em.getTransaction().commit();
        em.close();
        return users;        
    }
    public BugzillaUser getUser(String email){
        Logger.getLogger(this.getClass()).debug("Retrieving user");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        BugzillaUser user = (BugzillaUser) em.createNamedQuery("BUsers.getUserByEmail")
                .setParameter("email",email)
                .getSingleResult();
        em.getTransaction().commit();
        em.close();
        return user;                  
    }
}
