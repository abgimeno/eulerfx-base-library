/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans;

import euler.bugzilla.model.ServerInformation;
import java.io.IOException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

/**
 *
 * @author agime
 */
public class BugzillaCredentialsBean extends AbstractBean<ServerInformation> {

    public BugzillaCredentialsBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }

    public ServerInformation getCredentials() throws NoResultException {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        ServerInformation cred = (ServerInformation) em.createNamedQuery("SI.getAll").getSingleResult();
        em.getTransaction().commit();
        em.close();
        return cred;

    }
    
    public List<ServerInformation> getServerCredentials() throws NoResultException {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        List cred = em.createNamedQuery("SI.getAll").getResultList();
        em.getTransaction().commit();
        em.close();
        return cred;

    }    
    public ServerInformation getCredentials(String url) throws NoResultException {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        ServerInformation cred = (ServerInformation) em.createNamedQuery("SI.getByURL")
                                                        .setParameter("url", url)
                                                        .getSingleResult();
        em.getTransaction().commit();
        em.close();
        return cred;

    }     

}
