/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans;

import euler.bugzilla.model.Statistics;
import euler.bugzilla.model.StatsPK;
import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class StatsBean extends AbstractBean<Statistics>{

    public StatsBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }

    
    public Statistics find(Integer week, Integer year) {
        try {
            StatsPK id = new StatsPK(week, year);
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            Statistics stat = (Statistics) em.find(Statistics.class,id);
            em.getTransaction().commit();
            em.close();
            return stat;
        } catch (NoResultException ex) {
            Logger.getLogger(this.getClass()).error("No result returned for week "+ week+" of year "+year);
            Logger.getLogger(this.getClass()).error(null,ex);
            return null;
        }
    }
   
    public List<Statistics> getAll() {
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            List<Statistics> stats = em.createNamedQuery("Statistics.getAll").getResultList();
            em.getTransaction().commit();
            em.close();
            return stats;
        } catch (NoResultException ex) {
            Logger.getLogger(this.getClass()).error(null,ex);
            return null;
        }
    }
    
}
