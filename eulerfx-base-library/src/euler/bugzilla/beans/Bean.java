/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans;


public interface Bean<T> {

    public void remove(T item);
    public void persist(T item);
    public T merge(T item);

}
