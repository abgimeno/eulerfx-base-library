/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public final class PersistenceUtils {

    private static volatile PersistenceUtils instance;
    private Map<String, EntityManagerFactory> emfMap = new HashMap<>();
    private PersistenceUtils() {
    }
    public static PersistenceUtils getInstance() {
        //double-checked lock - safe from Java 5 onwards.
        if (instance == null) {
            synchronized (PersistenceUtils.class) {
                if (instance == null) {
                    instance = new PersistenceUtils();
                }
            }
        }
        return instance;
    }
    public boolean isConfigured(String persistenceUnitName) {
        return emfMap.containsKey(persistenceUnitName);
    }
    public EntityManagerFactory getEntityManagerFactory(String persistenceUnitName, Map<String, String> emfSettings) {
        if (emfMap.get(persistenceUnitName) == null) {
            synchronized (this) {
                if (emfMap.get(persistenceUnitName) == null) {
                    EntityManagerFactory emf = null;
                    if (emfSettings == null) {
                        emf = Persistence.createEntityManagerFactory(persistenceUnitName);
                    } else {
                        emf = Persistence.createEntityManagerFactory(persistenceUnitName, emfSettings);
                    }
                    emfMap.put(persistenceUnitName, emf);
                }
            }
        }
        return emfMap.get(persistenceUnitName);
    }
}