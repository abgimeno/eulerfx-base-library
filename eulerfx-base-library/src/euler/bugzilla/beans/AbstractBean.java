/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans;


import java.io.IOException;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

/**
 *
 * @author root
 */
public abstract class AbstractBean<T extends Serializable> implements Bean<T> {
    
    protected EntityManagerFactory emf;
    
    public AbstractBean(String persistenceUnitName) throws IOException {
        emf = PersistenceUtils.getInstance().getEntityManagerFactory(persistenceUnitName, null);
    }
    
    @Override
    public void remove(T item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.merge(item));
        em.getTransaction().commit();
        em.close();
    }
    @Override
    public void persist(T item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(item);
        em.getTransaction().commit();
        em.close();
    }
    @Override
    public T merge(T item) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        T merged = em.merge(item);
        em.getTransaction().commit();
        em.close();
        return merged;
    }
    
}
