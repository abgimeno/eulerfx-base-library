/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.beans.conf;

import euler.bugzilla.beans.AbstractBean;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.conf.QueryTableColumn;
import euler.bugzilla.model.conf.QueryTableConf;
import euler.bugzilla.model.query.Query;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class BugTableBean extends AbstractBean<QueryTableConf> {

    public BugTableBean(String persistenceUnitName) throws IOException {
        super(persistenceUnitName);
    }

    public QueryTableConf findConfiguration(Query query) {
        Query dbQuery = null;
        try {
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();
            dbQuery = em.find(Query.class, query.getId());
            QueryTableConf conf = (QueryTableConf) em.createNamedQuery("QueryTableConf.getConfiguration")
                    .setParameter("query", dbQuery).getSingleResult();
            em.getTransaction().commit();
            em.close();
            return conf;
        } catch (NoResultException ex) {
            QueryTableConf qtconf = setDefaultTableConfiguration(dbQuery);
            persist(qtconf);
            Logger.getLogger(this.getClass()).error("No configuration found for " + query.getName(), ex);
            return qtconf;
        }
    }

    public void persist(QueryTableColumn columnInfo) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(columnInfo);
        em.getTransaction().commit();
        em.close();
    }

    public QueryTableColumn find(String columnName, Query query) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();      
        QueryTableColumn conf = (QueryTableColumn) em.createNamedQuery("QueryTableColumn.getByName")
                .setParameter("name", columnName)
                .setParameter("query", query)
                .getSingleResult();
        em.getTransaction().commit();
        em.close();
        return conf;
    }

    public void merge(QueryTableColumn columnInfo) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(columnInfo);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void remove(QueryTableConf configuration) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(configuration);
        em.getTransaction().commit();
        em.close();
    }

    private QueryTableConf setDefaultTableConfiguration(Query query) {
        QueryTableConf qtconf = new QueryTableConf();
        List<QueryTableColumn> columns = new LinkedList<>();
        Logger.getLogger(this.getClass()).debug("Could not find a configuration for query " + query.getName());
        for (String columnName : BugInfo.getBugKeys()) {
            QueryTableColumn column = new QueryTableColumn(columnName);            
            String[] defaultKeys = BugInfo.getDefaultKeys();
            for(int i=0; i<defaultKeys.length;i++){
               String defaultKey = defaultKeys[i];
               if(defaultKey.equals(columnName)){
                   column.setVisible(true);
                   column.setPosition(i);
               }
            }           
            persist(column);
            columns.add(column);
        }
        qtconf.setColumns(columns);
        qtconf.setQuery(query);        
        return qtconf;
    }

}
