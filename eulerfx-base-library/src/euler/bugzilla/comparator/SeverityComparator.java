/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.comparator;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author agime
 */
public class SeverityComparator implements Comparator<String>{
    
    private static Map<String, Integer> severities = new HashMap<>();
    static {
        Map<String, Integer>  aMap = new HashMap<>();
        aMap.put("blocker", 10);
        aMap.put("critical", 9);
        aMap.put("major", 8);
        aMap.put("normal", 7);
        aMap.put("minor", 6);
        aMap.put("trivial", 5);
        aMap.put("enhancement", 4);
        severities = Collections.unmodifiableMap(aMap);

    }
    ;        
    @Override
    public int compare(String o1, String o2) {
        return Integer.compare(severities.get(o1), severities.get(o2));
    }
    
}
