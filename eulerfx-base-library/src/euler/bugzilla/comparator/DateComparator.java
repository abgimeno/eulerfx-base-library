/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.comparator;


import euler.bugzilla.model.BugInfo;
import euler.bugzilla.utils.ConcurrentDateFormatAccess;
import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class DateComparator implements Comparator<String>{

    @Override
    public int compare(String t, String t1) {
        try {            
            ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess(BugInfo.DATEFORMAT); 
            //format.setTimeZone(TimeZone.getTimeZone("EEST""));
            Date d1 = df.stringToDate(t);
            Date d2 = df.stringToDate(t1);
            return Long.compare(d1.getTime(), d2.getTime());
        } catch (ParseException p) {
            Logger.getLogger(this.getClass()).error(p);
        } catch(NullPointerException ex){
            Logger.getLogger(this.getClass()).debug(getNullMessagge(t, t1));
        }
        return -1;

    }

    private String getNullMessagge(String t, String t1) {
        StringBuilder sb = new StringBuilder();
        sb.append("Null date in comparation. t= ");
        sb.append(t);
        sb.append(" t1= ");
        sb.append(t1);
        return sb.toString();
    }
           
}
