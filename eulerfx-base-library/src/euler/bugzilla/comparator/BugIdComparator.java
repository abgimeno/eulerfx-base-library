/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.comparator;

import java.util.Comparator;

/**
 *
 * @author agime
 */
public class BugIdComparator implements Comparator<String>{

    @Override
    public int compare(String o1, String o2) {
        return Long.compare(Long.parseLong(o1),Long.parseLong(o2));    
    }
    
}
