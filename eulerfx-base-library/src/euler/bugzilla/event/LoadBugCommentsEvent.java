/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.event;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author agime
 */
public class LoadBugCommentsEvent extends Event {
    
    public static final EventType<LoadBugCommentsEvent> LOAD_BUGS = new EventType<>(Event.ANY, "LOAD");
    public static final EventType<LoadBugCommentsEvent> BUGS_LOADED = new EventType<>(Event.ANY, "LOADED");

    private Integer bugId;
    
    public LoadBugCommentsEvent(Integer bugId, EventType<? extends Event> et) {
        super(et);
        this.bugId=bugId;
    }

    public LoadBugCommentsEvent(Integer bugId, Object o, EventTarget et, EventType<? extends Event> et1) {
        super(o, et, et1);
        this.bugId=bugId;
    }

    public Integer getBugId() {
        return bugId;
    }
    
}
