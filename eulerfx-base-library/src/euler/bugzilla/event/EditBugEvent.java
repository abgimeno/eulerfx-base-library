/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.event;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author agime
 */
public class EditBugEvent extends Event {
    
    public static final EventType<EditBugEvent> EDIT = new EventType<>(Event.ANY, "EDIT");
    private Integer bugId;
        
    public EditBugEvent(Integer bugId, EventType<? extends Event> et) {
        super(et);
        this.bugId=bugId;
    }

    public EditBugEvent(Integer bugId, EventTarget et, EventType<? extends Event> et1) {
        super(bugId, et, et1);
        this.bugId=bugId;        
    }

    public Integer getBugId() {
        return bugId;
    }
    
}
