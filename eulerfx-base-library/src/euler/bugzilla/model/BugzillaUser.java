/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model;

import euler.bugzilla.j2bugzilla.enums.BugzillaUserParams;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author agime
 */
@Entity
@Access(AccessType.PROPERTY)
@NamedQueries({
        @NamedQuery(name="BUsers.getUsers", query="SELECT bu FROM BugzillaUser bu"),
        @NamedQuery(name="BUsers.getUserByEmail", query="SELECT bu FROM BugzillaUser bu WHERE bu.email =:email")
})
public class BugzillaUser implements Serializable {
 
    private Map<BugzillaUserParams, Object> internalState = new HashMap<>();
    
    private Integer id;//    int The unique integer ID that Bugzilla uses to represent this user. Even if the user's login name changes, this will not change.    
    private String realName;//string The actual name of the user. May be blank.
    private String email;//string The email address of the user.
    private String name;//string The login name of the user. Note that in some situations this is different than their email.
    private boolean canLogin;//boolean A boolean value to indicate if the user can login into bugzilla.
    private boolean emailEnabled;// boolean A boolean value to indicate if bug-related mail will be sent to the user or not.
    private String loginDeniedText;//string A text field that holds the reason for disabling a user from logging into bugzilla, if empty then the user account is enabled. Otherwise it is disabled/closed.        
    
    
    
    private ServerInformation server;
    
    private List groups;
    /* array An array of group hashes the user is a member of. If the currently logged in user is querying his own account or is a member of the 'editusers' group, the array will contain all the groups that the user is a member of. Otherwise, the array will only contain groups that the logged in user can bless. Each hash describes the group and contains the following items:
                id int The group id
                name string The name of the group
                description string The description for the group*/

   // private List savedSearches;
    /*array An array of hashes, each of which represents a user's saved search and has the following keys:
            id int An integer id uniquely identifying the saved search.
           name string The name of the saved search.
           query string The CGI parameters for the saved search.*/

   // private List savedReports;

    public BugzillaUser(int id, String realName, String email, String name, boolean canLogin, List groups) {
        this.id = id;
        this.realName = realName;
        this.email = email;
        this.name = name;
        this.canLogin = canLogin;
        //this.groups = groups;
    }

    public BugzillaUser() {        
    }

    /*array An array of hashes, each of which represents a user's saved report and has the following keys:
    id int An integer id uniquely identifying the saved report.
    name string The name of the saved report.
    query tring The CGI parameters for the saved report.*/
    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column
    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }
    @Column
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Transient
    public boolean getCanLogin() {
        return canLogin;
    }

    public void setCanLogin(boolean canLogin) {
        this.canLogin = canLogin;
    }
    @Transient
    public boolean getEmailEnabled() {
        return emailEnabled;
    }

    public void setEmailEnabled(boolean emailEnabled) {
        this.emailEnabled = emailEnabled;
    }
    @Transient
    public String getLoginDeniedText() {
        return loginDeniedText;
    }

    public void setLoginDeniedText(String loginDeniedText) {
        this.loginDeniedText = loginDeniedText;
    }
    
    @Transient
    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List groups) {
        this.groups = groups;
    }

   /* public List getSavedSearches() {
        return savedSearches;
    }

    public void setSavedSearches(List savedSearches) {
        this.savedSearches = savedSearches;
    }

    public List getSavedReports() {
        return savedReports;
    }

    public void setSavedReports(List savedReports) {
        this.savedReports = savedReports;
    }*/
    
    @Transient
    public Map<BugzillaUserParams, Object> getInternalState() {
        return internalState;
    }

    public void setInternalState(Map<BugzillaUserParams, Object> internalState) {
        this.internalState = internalState;
    }

    @Transient
    public ServerInformation getServer() {
        return server;
    }

    public void setServer(ServerInformation server) {
        this.server = server;
    }
    
}
