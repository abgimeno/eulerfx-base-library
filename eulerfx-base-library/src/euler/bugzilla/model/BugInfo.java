/*
* To change this template, choose Tools | Templates and open the template in the editor.
*/
 
package euler.bugzilla.model;

import euler.bugzilla.utils.ConcurrentDateFormatAccess;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
@Entity
@Access(AccessType.PROPERTY)
public class BugInfo implements Serializable, Cloneable {
    
    public static final String OPENEDTHISWEEK = "openedThisWeek";
    public static final String STYLE_THISWEEK = "openedThisWeek";
    public static final String STYLE_BLOCKER = "blocker";
    public static final String DATEFORMAT = "dd/MM/yyyy HH:mm";    
    
    public static final String ALIAS = "alias";
    public static final String ASSIGNEE = "assigned_to";
    public static final String BLOCKS = "blocks";
    public static final String CC = "cc";
    public static final String CLASSIFICATION = "classification";
    public static final String COMPLETE = "percentage_complete";
    public static final String COMPONENT = "component";
    public static final String CREATOR = "creator";
    public static final String DEADLINE = "deadline";
    public static final String DEPENDS_ON = "depends_on";
    public static final String DESCRIPTION = "description";
    public static final String ESTIMATED = "estimated_time";
    public static final String ID = "id";
    public static final String LAST_CHANGE = "last_change_time";
    public static final String MILESTONE = "target_milestone";
    public static final String OPEN_ON = "creation_time";    
    public static final String OS = "op_sys";
    public static final String PLATFORM = "platform";
    public static final String PRIORITY = "priority";
    public static final String PRODUCT = "product";
    public static final String REMAINING = "remaining_time";
    public static final String RESOLUTION = "resolution";
    public static final String SEVERITY = "severity";
    public static final String SHORTDESC = "short_desc";
    public static final String STATUS = "status";
    public static final String SUMMARY = "summary";
    public static final String URL = "url";
    public static final String VERSION = "version";
    public static final String WHITEBOARD = "whiteboard";
    
    private static final String[] keys ={ALIAS, ASSIGNEE,BLOCKS,CC,CLASSIFICATION,COMPLETE,COMPONENT,CREATOR, DEADLINE, DEPENDS_ON, DESCRIPTION,
                            ESTIMATED,ID,LAST_CHANGE,MILESTONE,OPEN_ON,OS,PLATFORM,PRIORITY,PRODUCT, RESOLUTION,
                             SEVERITY, SHORTDESC,STATUS,SUMMARY,URL, VERSION,REMAINING,WHITEBOARD};
    
    public static final String[] defaultKeys ={ID,SUMMARY,STATUS,RESOLUTION,SEVERITY, PRODUCT,
                                              ASSIGNEE,PRIORITY,ESTIMATED,DEADLINE};

    private int NKEYS=29;
    private Map<String, Property> internalState = new LinkedHashMap<>();    
    private BooleanBinding openedThisWeek, modifiedThisWeek;
    private static final String UNDERSCORE = "_";
    private static final String BLANK_SPACE = " ";
        
    public BugInfo() {        
        internalState.put(ALIAS, new SimpleStringProperty());
        internalState.put(ASSIGNEE, new SimpleStringProperty());
        internalState.put(BLOCKS, new SimpleListProperty());
        internalState.put(CC, new SimpleListProperty());
        internalState.put(CLASSIFICATION, new SimpleStringProperty());
        internalState.put(COMPLETE, new SimpleStringProperty());
        internalState.put(COMPONENT, new SimpleStringProperty());        
        internalState.put(CREATOR, new SimpleStringProperty());   
        internalState.put(DEADLINE, new SimpleStringProperty());        
            internalState.put(DEPENDS_ON, new SimpleListProperty());
        internalState.put(DESCRIPTION, new SimpleStringProperty());       
        internalState.put(ESTIMATED, new SimpleStringProperty());        
        internalState.put(ID, new SimpleIntegerProperty());
        internalState.put(LAST_CHANGE, new SimpleStringProperty());
        internalState.put(MILESTONE, new SimpleStringProperty());        
        internalState.put(OPEN_ON, new SimpleStringProperty());
        internalState.put(OS, new SimpleStringProperty());
        internalState.put(PLATFORM, new SimpleStringProperty());
        internalState.put(PRIORITY, new SimpleStringProperty());
        internalState.put(PRODUCT, new SimpleStringProperty());
        internalState.put(RESOLUTION, new SimpleStringProperty());                
        internalState.put(SEVERITY, new SimpleStringProperty());
        internalState.put(SHORTDESC, new SimpleStringProperty());
        internalState.put(STATUS, new SimpleStringProperty());                
        internalState.put(SUMMARY, new SimpleStringProperty());
        internalState.put(URL, new SimpleStringProperty());       
        internalState.put(VERSION, new SimpleStringProperty());
        internalState.put(REMAINING, new SimpleStringProperty());
        internalState.put(WHITEBOARD, new SimpleStringProperty());                        
        openedThisWeek = isDateFieldWihtinThisWeekBinding(OPEN_ON);
        modifiedThisWeek = isDateFieldWihtinThisWeekBinding(LAST_CHANGE);
    }

    public BugInfo(Bug bug) {
        this();
        try {            
            ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess(DATEFORMAT);
            Map<String, Object> bugMap = bug.getInternalState();
            for (String key : bugMap.keySet()) {
                if (bugMap.get(key) instanceof String) {
                    String value = (String) bugMap.get(key);
                    if (value.equals(ID)) { // id gets converted to Integer
                        IntegerProperty number = new SimpleIntegerProperty();
                        number.setValue(Integer.parseInt(value));
                        internalState.put(key, number);
                    } else {
                        StringProperty text = new SimpleStringProperty();
                        text.setValue(value);
                        internalState.put(key, text);
                    }
                }
                if (bugMap.get(key) instanceof Integer) {
                    IntegerProperty number = new SimpleIntegerProperty();
                    number.setValue((Integer) bugMap.get(key));
                    internalState.put(key, number);
                }
                if (bugMap.get(key) instanceof Date) {
                    StringProperty text = new SimpleStringProperty();
                    Date date = (Date) bugMap.get(key);
                    text.setValue(df.dateToString(date));
                    internalState.put(key, text);
                }
                if (bugMap.get(key) instanceof Object[]) {
                    ListProperty items = new SimpleListProperty(FXCollections.observableArrayList());
                    items.addAll(Arrays.asList((Object[]) bugMap.get(key)));
                    internalState.put(key, items);
                }
            }

        openedThisWeek = isDateFieldWihtinThisWeekBinding(OPEN_ON);
        modifiedThisWeek = isDateFieldWihtinThisWeekBinding(LAST_CHANGE);

        } catch (NumberFormatException ex) {
            Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
        }
    }

    @Id
    public Integer getBugId() {
        return (Integer) internalState.get(ID).getValue();
    }

    public String getId() {
        return Integer.toString(getBugId());
    }

    public void setBugId(Integer bugId) {
        internalState.get(ID).setValue(bugId);
    }

    @Column
    public String getCreationTime() {
        return (String) internalState.get(OPEN_ON).getValue();
    }

    public void setCreationTime(String creationTime) {
        internalState.get(OPEN_ON).setValue(creationTime);
    }

    @Column
    public String getBugSeverity() {
        return (String) internalState.get(SEVERITY).getValue();
    }

    public void setBugSeverity(String bugSeverity) {
        internalState.get(SEVERITY).setValue(bugSeverity);
    }

    @Column
    public String getBugStatus() {
        return (String) internalState.get(STATUS).getValue();
    }

    public String getStatus() {
        return getBugStatus();
    }

    public void setBugStatus(String bugStatus) {
        internalState.get(STATUS).setValue(bugStatus);
    }

    @Column
    public String getSummary() {
        return (String) internalState.get(SUMMARY).getValue();
    }

    public void setSummary(String summary) {
        internalState.get(SUMMARY).setValue(summary);
    }

    public String getShortDesc() {
        return (String) internalState.get(SHORTDESC).getValue();
    }

    public void setShortDesc(String shortDesc) {
        internalState.get(SUMMARY).setValue(shortDesc);
    }

    @Column
    public String getProduct() {
        return (String) internalState.get(PRODUCT).getValue();
    }

    public void setProduct(String product) {
        internalState.get(PRODUCT).setValue(product);
    }

    @Column
    public String getAssignedTo() {
        return (String) internalState.get(ASSIGNEE).getValue();
    }

    public void setAssignedTo(String assignedTo) {
        internalState.get(ASSIGNEE).setValue(assignedTo);
    }

    @Column
    public String getPriority() {
        return (String) internalState.get(PRIORITY).getValue();
    }

    public void setPriority(String priority) {
        internalState.get(PRIORITY).setValue(priority);
    }

    @Column
    public String getRemainingTime() {
        return (String) internalState.get(REMAINING).getValue();
    }

    public void setRemainingTime(String remainingTime) {
        internalState.get(REMAINING).setValue(remainingTime);
    }

    @Column
    public String getEstimatedTime() {
        return (String) internalState.get(ESTIMATED).getValue();
    }

    public void setEstimatedTime(String estimatedTime) {
        internalState.get(ESTIMATED).setValue(estimatedTime);
    }

    @Column
    public String getDeadline() {
        return (String) internalState.get(DEADLINE).getValue();
    }

    public void setDeadline(String deadline) {
        internalState.get(DEADLINE).setValue(deadline);
    }

    @Column
    public String getPercentageComplete() {
        return (String) internalState.get(COMPLETE).getValue();
    }

    public void setPercentageComplete(String percentageComplete) {
        internalState.get(COMPLETE).setValue(percentageComplete);
    }
    @Column
    public String getCreator() {
        return (String) internalState.get(CREATOR).getValue();
    }

    public void setCreator (String creator) {
        internalState.get(CREATOR).setValue(creator);
    }

    @Column
    public String getResolution() {
        return (String) internalState.get(RESOLUTION).getValue();
    }

    public void setResolution(String resolution) {
        internalState.get(RESOLUTION).setValue(resolution);
    }

    @ElementCollection
    @CollectionTable(name ="DEPENDENCES")
    public List<Integer> getDependsOn() {
        return (List<Integer>) internalState.get(DEPENDS_ON).getValue();
    }

    public void setDependsOn(List<Integer> dependences) {
        ListProperty items = new SimpleListProperty(FXCollections.observableArrayList());
        items.addAll(dependences);          
        internalState.get(DEPENDS_ON).setValue(items);
    }

    @Column
    public String getOpsys() {
        return (String) internalState.get(OS).getValue();
    }

    public void setOpsys(String os) {
        internalState.get(OS).setValue(os);
    }

    public void setCC(List<String> list) {
        ObservableList ovList = FXCollections.<String>observableArrayList();
        ovList.addAll(list);
        internalState.get(CC).setValue(ovList);
    }

    @Column
    public String getLastChangeTime(){
        return (String) internalState.get(LAST_CHANGE).getValue();
    }
    
    public void setLastChangeTime(String lastChange){
         internalState.get(LAST_CHANGE).setValue(lastChange);
    }
    
    @ElementCollection
    @CollectionTable(name = "BUG_CC")
    public List<String> getCC() {
        return (List<String>) internalState.get(CC);
    }
    
    @Column
    public String getUrl(){
        return (String) internalState.get(URL).getValue();
    }
    
    public void setUrl(String url){
        internalState.get(URL).setValue(url);
    }
    
    @Transient
    public Map<String, Property> getInternalState() {
        return internalState;
    }

    public void setInternalState(Map<String, Property> internalState) {
        this.internalState = internalState;
    }
    ////////////////properties\\\\\\\\\\\\\\\\\

    public StringProperty getProperty(String key) {
        return (StringProperty) internalState.get(key);
    }

    public StringProperty bugIdProperty() {
        return (StringProperty) internalState.get(ID);
    }

    public StringProperty creationTimeProperty() {
        return (StringProperty) internalState.get(OPEN_ON);
    }
    public StringProperty creatorProperty() {
        return (StringProperty) internalState.get(CREATOR);
    }

    public StringProperty bugSeverityProperty() {
        return (StringProperty) internalState.get(SEVERITY);
    }

    public StringProperty bugStatusProperty() {
        return (StringProperty) internalState.get(STATUS);
    }

    public StringProperty shortDescProperty() {
        return (StringProperty) internalState.get(SHORTDESC);
    }

    public StringProperty productProperty() {
        return (StringProperty) internalState.get(PRODUCT);
    }

    public StringProperty assignedToProperty() {
        return (StringProperty) internalState.get(ASSIGNEE);
    }

    public StringProperty priorityProperty() {
        return (StringProperty) internalState.get(PRIORITY);
    }

    public StringProperty remainingTimeProperty() {
        return (StringProperty) internalState.get(REMAINING);
    }

    public StringProperty estimatedTimeProperty() {
        return (StringProperty) internalState.get(ESTIMATED);
    }

    public StringProperty deadlineProperty() {
        return (StringProperty) internalState.get(DEADLINE);
    }

    public StringProperty percentageCompleteProperty() {
        return (StringProperty) internalState.get(COMPLETE);
    }

    public SimpleStringProperty componentProperty() {
        return (SimpleStringProperty) internalState.get(COMPONENT);
    }

    public SimpleStringProperty platformProperty() {
        return (SimpleStringProperty) internalState.get(PLATFORM);
    }

    public SimpleStringProperty opsysProperty() {
        return (SimpleStringProperty) internalState.get(OS);
    }

    public SimpleStringProperty resolutionProperty() {
        return (SimpleStringProperty) internalState.get(RESOLUTION);
    }

    public SimpleListProperty dependsOnProperty() {
        return (SimpleListProperty) internalState.get(DEPENDS_ON);
    }

    public SimpleStringProperty aliasProperty() {
        return (SimpleStringProperty) internalState.get(ALIAS);
    }

    public SimpleStringProperty summaryProperty() {
        return (SimpleStringProperty) internalState.get(SUMMARY);
    }

    public SimpleListProperty blocksProperty() {
        return (SimpleListProperty) internalState.get(BLOCKS);
    }

    public SimpleStringProperty versionProperty() {
        return (SimpleStringProperty) internalState.get(VERSION);
    }

    public SimpleListProperty ccProperty() {
        return (SimpleListProperty) internalState.get(CC);
    }

    public SimpleStringProperty descriptionProperty() {
        return (SimpleStringProperty) internalState.get(DESCRIPTION);
    }
    
    public SimpleStringProperty urlProperty(){
        return (SimpleStringProperty) internalState.get(URL);
    }
    
    @Transient
    public Boolean getOpenedThisWeek() {
        return this.openedThisWeek.get();
    }
    
    @Transient
    public Boolean getModifiedThisWeek() {
        return modifiedThisWeek.get();
    }

    public void setModifiedThisWeek(Boolean modifiedThisWeek) {
        
    }
    

    public void setOpenedThisWeek(Boolean opened) {

    }

    private BooleanBinding isDateFieldWihtinThisWeekBinding(final String dateFieldName) {
        return new BooleanBinding() {
            {
                super.bind(internalState.get(dateFieldName));
            }

            @Override
            protected boolean computeValue() {
                try {
                    //Logger.getLogger(this.getClass())"computing value ...");
                    Calendar c = Calendar.getInstance();
                    c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                    c.set(Calendar.HOUR_OF_DAY, 0);
                    ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess(DATEFORMAT);
                    Date opened = df.stringToDate((String) internalState.get(dateFieldName).getValue());
                    /*Logger.getLogger(this.getClass())"is "+df.parse(openDate.get()) +" after than "+c.getTime()+
                     "? "+opened.after(c.getTime()));*/
                    return (opened.after(c.getTime()));
                } catch (ParseException ex) {
                    Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
                    return false;
                }

            }
        };
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        for (String key : internalState.keySet()) {
            output.append("\"");
            output.append(internalState.get(key).getValue());
            output.append("\"");
            output.append(",");
        }
        if (!output.toString().isEmpty()) {
            output.deleteCharAt(output.length() - 1);
            output.append("\n");
        }

        return output.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public List<String> getColumns() {
        List<String> list = new ArrayList<>();
        list.addAll(internalState.keySet());
        return list;
    }

    @Column
    public String getComponent() {
        return (String) internalState.get(COMPONENT).getValue();
    }

    @Column
    public String getMilestone() {
        return (String) internalState.get(MILESTONE).getValue();
    }

    @Column
    public String getPlatform() {
        return (String) internalState.get(PLATFORM).getValue();
    }

    @Column
    public String getSeverity() {
        return (String) internalState.get(SEVERITY).getValue();
    }

    @Column
    public String getVersion() {
        return (String) internalState.get(VERSION).getValue();
    }

    @Column(name="BUG_ALIAS")
    public String getAlias() {
        return internalState.containsKey(ALIAS)? (String) internalState.get(ALIAS).getValue():
                                                  "";
    }

    @ElementCollection
    @CollectionTable(name ="BLOCKS")
    public List<Integer> getBlocks() {
        return (List<Integer>) internalState.get(BLOCKS).getValue();
    }

    public void setComponent(String component) {
        internalState.get(COMPONENT).setValue(component);
    }

    public void setMilestone(String milestone) {
        internalState.get(MILESTONE).setValue(milestone);
    }

    public void setPlatform(String platform) {
        internalState.get(PLATFORM).setValue(platform);
    }

    public void setSeverity(String severity) {
        internalState.get(SEVERITY).setValue(severity);
    }

    public void setVersion(String version) {
        internalState.get(VERSION).setValue(version);
    }

    public void setAlias(String alias) {
        internalState.get(ALIAS).setValue(alias);
    }

    public void setBlocks(List<Integer> blocks) {
        ListProperty items = new SimpleListProperty(FXCollections.observableArrayList());
        items.addAll(blocks);        
        internalState.get(BLOCKS).setValue(items);
    }
    public static String[] getBugKeys(){
        return keys;
    }
    public static String[] getDefaultKeys(){
        return defaultKeys;
    }

    public static String generateReadableName(String key) {
        String firstChar = String.valueOf(key.charAt(0));
        StringBuilder sb = new StringBuilder(key);
        sb.replace(0, 1, firstChar.toUpperCase());
        if(needsProcessing(sb.toString())){
            return doProcessing(sb.toString());
        }
        return sb.toString();
            
    }

    private static boolean needsProcessing(String key) {
        return key.contains(UNDERSCORE);
    }
    /*
    replaces underscore with blank space and capitalises first letter
    */
    private static String doProcessing(String key) {        
        StringBuilder sb = new StringBuilder(key);
        while(sb.toString().contains(UNDERSCORE)){
            int underscorePos = sb.lastIndexOf(UNDERSCORE);
            if(underscorePos < sb.length()){
                String nextChar = String.valueOf(sb.charAt(underscorePos+1));                
                sb.replace(underscorePos, underscorePos+1, BLANK_SPACE);
                sb.replace(underscorePos+1, underscorePos+2, nextChar.toUpperCase());                
            }
        }
        return sb.toString();        
    }    

    public static String generatePropertyName(String key) {
        if(needsProcessing(key)){
            return generateName(key);
        }
        return key;    
    }     
    
    private static String generateName(String key) {        
        StringBuilder sb = new StringBuilder(key);
        while(sb.toString().contains(UNDERSCORE)){
            int underscorePos = sb.lastIndexOf(UNDERSCORE);
            if(underscorePos < sb.length()){
                String nextChar = String.valueOf(sb.charAt(underscorePos+1));                
                sb.deleteCharAt(underscorePos);
                sb.replace(underscorePos, underscorePos+1, nextChar.toUpperCase());                
            }
        }
        return sb.toString();        
    }       

    public static boolean isDate(String propertyName) {
        return propertyName.equals(BugInfo.LAST_CHANGE) || propertyName.equals(BugInfo.OPEN_ON);
    }    
}
