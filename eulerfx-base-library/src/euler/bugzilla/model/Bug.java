package euler.bugzilla.model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import euler.bugzilla.j2bugzilla.Flag;
import euler.bugzilla.j2bugzilla.Flag.Status;
import static euler.bugzilla.model.BugInfo.ALIAS;
import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.persistence.*;
import org.apache.log4j.Logger;

/**
 * This class encapsulates all information about a bug report posted on a
 * Bugzilla installation. It provides getter methods for various properties,
 * such as the bug summary and status. To obtain a new {@literal Bug} object,
 * you must use the {@link BugFactory} class which provides a fluent interface
 * for bug creation. Note that the {@literal BugFactory} does not submit a
 * report for you -- to actually add the created bug to your Bugzilla
 * installation, you must use the {@link ReportBug} method.
 *
 * The j2bugzilla original file has been modified
 *
 */
@Entity
public class Bug implements Serializable {

    /**
     * The keys which <em>must</em> have non-blank values for a bug to be
     * properly submitted
     */
    private static String[] requiredKeys = {BugInfo.PRODUCT, BugInfo.COMPONENT, BugInfo.SUMMARY, BugInfo.VERSION};
    /**
     * HashMap representing fields for each Bug. The Value for each Key is a
     * <code>String</code>
     * <em>except</em> for the CC: field, which is an array of
     * <code>Strings</code>.
     */
    private Map<String, Object> internalState;

    public Bug() {
    }

    /**
     * Constructor for creating a new {@link Bug} to submit to an installation.
     * The constructor ensures any required values in {@link #requiredKeys} are
     * set, and throws an {@link IllegalStateException} if they are null.
     *
     * @param state A <code>Map</code> pairing required keys to values
     */
    public Bug(Map<String, Object> state) {
        checkRequiredFields(state);
        internalState = state;
    }

    /**
     * Internal method for determining whether a given <code>HashMap</code> is a
     * valid representation of a {@link Bug} or not.
     *
     * @param state a collection of String keys and String values in * a
     * <code>HashMap</code>
     * @throws IllegalStateException If a required key-value pair is null
     */
    private void checkRequiredFields(Map<String, Object> state) {
        for (String str : requiredKeys) {
            if (!state.containsKey(str)) {
                Logger.getLogger(this.getClass()).error("Missing key/value pair: " + str);
            }
        }
    }

    /**
     * Returns how highly this bug is ranked. Since this field can be edited
     * between installations, you may wish to
     * {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * @return a {@literal String} describing the relative importance of this
     * bug
     */
    @Column
    public String getPriority() {
        return (String) internalState.get(BugInfo.PRIORITY);
    }

    public void setPriority(String priority) {
        internalState.put(BugInfo.PRIORITY, priority);
    }

    /**
     * Returns How severe the bug is, or whether it's an enhancement. Since this
     * field can be edited between installations, you may wish to
     * {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * @return a {@literal String} describing the relative severity of this bug
     */
    @Column
    public String getSeverity() {
        return (String) internalState.get(BugInfo.SEVERITY);
    }

    public void setSeverity(String severity) {
        internalState.put(BugInfo.SEVERITY, severity);
    }

    /**
     * Returns the internal Bugzilla ID number for this bug. If it is not in the
     * Bugzilla database, this will return null.
     *
     * @return integer ID
     */
    @Id
    public int getID() {
        return (Integer) internalState.get(BugInfo.ID);
    }

    public void setID(int id) {
        internalState.put(BugInfo.ID, id);
    }

    /**
     * Returns the unique alias of this {@link Bug}. If none is set, this method
     * will return null.
     *
     * @return A {@literal String} representing the unique alias for this bug.
     */
    @Column
    public String getAlias() {
        return internalState.containsKey(ALIAS) ? (String) internalState.get(ALIAS)
                                                    : "";
    }

    /**
     * Sets the alias of this {@link Bug}. By default, Bugzilla restricts
     * aliases to be 20 characters in length.
     *
     * @param alias A {@literal String} representing a unique alias for this
     * bug.
     */
    public void setAlias(String alias) {
        internalState.put(BugInfo.ALIAS, alias);
    }

    /**
     * Returns the one-line summary included with the original bug report.
     *
     * @return A {@literal String} representing the summary entered for this
     * {@link Bug}.
     */
    @Column
    public String getSummary() {
        return (String) internalState.get(BugInfo.SUMMARY);
    }

    /**
     * Sets the one-line summary of this {@link Bug}.
     *
     * @param summary A {@literal String} representing the summary describing
     * this bug.
     */
    public void setSummary(String summary) {
        internalState.put(BugInfo.SUMMARY, summary);
    }

    @Column
    public String getShortDesc() {
        return (String) internalState.get(BugInfo.SHORTDESC);
    }

    public void setShortDesc(String shortDesc) {
        internalState.put(BugInfo.SHORTDESC, shortDesc);
    }

    /**
     * Returns the product this {@link Bug} belongs to.
     *
     * @return the Product category this {@link Bug} is filed under.
     */
    @Column
    public String getProduct() {
        return (String) internalState.get(BugInfo.PRODUCT);
    }

    /**
     * Sets the product this {@link Bug} is associated with. Note that a
     * nonexistent product name will result in an error from Bugzilla upon bug
     * submission.
     *
     * @param product A {@literal String} representing the product name.
     */
    public void setProduct(String product) {
        internalState.put(BugInfo.PRODUCT, product);
    }

    /**
     * Returns the component this {@link Bug} is associated with.
     *
     * @return the component of the {@link Bug}'s parent Product
     */
    @Column
    public String getComponent() {
        return (String) internalState.get(BugInfo.COMPONENT);
    }

    /**
     * Sets the component this {@link Bug} is associated with. Note that a
     * nonexistent component name will result in Bugzilla returning an error
     * upon submission. Since this field can be edited between installations,
     * you may wish to
     * {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * @param component A {@literal String} representing the component name.
     */
    public void setComponent(String component) {
        internalState.put(BugInfo.COMPONENT, component);
    }

    /**
     * Returns the version number of the product this {@link Bug} is associated
     * with.
     *
     * @return the version associated with this {@link Bug}
     */
    @Column
    public String getVersion() {
        return (String) internalState.get(BugInfo.VERSION);
    }

    /**
     * Sets the version number of the product this {@link Bug} is associated
     * with. Note that a nonexistent version number will result in Bugzilla
     * returning an error on submission. Since this field can be edited between
     * installations, you may wish to
     * {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * @param version A {@literal String} describing the version number of the
     * product affected by this bug.
     */
    public void setVersion(String version) {
        internalState.put(BugInfo.VERSION, version);
    }

    /**
     * Returns the status of this {@link Bug} indicating whether it is open or
     * closed.
     *
     * @return A {@literal String} representing the status of a {@link Bug}.
     */
    @Column
    public String getStatus() {
        return (String) internalState.get(BugInfo.STATUS);
    }

    /**
     * Sets the status of this {@link Bug} indicating whether it is open or
     * closed. Since this field can be edited between installations, you may
     * wish to {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * If changing a bug's state from closed to open, the resolution must also
     * be reset. Otherwise, the remote Bugzilla installation will throw an
     * error. Clients must manually call {@link #clearResolution()}. Since the
     * status and resolution fields are customizable, this library cannot safely
     * determine when to call this method automatically, so managing the state
     * of the bug is the responsibility of the caller.
     *
     * @param status A {@literal String} representing the status of this bug.
     * @see {@link #setResolution(String)}
     */
    public void setStatus(String status) {
        internalState.put(BugInfo.STATUS, status);
    }

    /**
     * Returns the resolution of this {@link Bug} if it is closed, or null if it
     * is still open.
     *
     * @return A {@literal String} representing the resolution of a {@link Bug}.
     * @see {@link @link com.j2bugzilla.rpc.GetLegalValues GetLegalValues} to
     * retrieve a list of the defined resolutions for a specific installation.
     */
    @Column
    public String getResolution() {
        return (String) internalState.get(BugInfo.RESOLUTION);
    }

    /**
     * Sets the resolution of this {@link Bug}. Since this field can be edited
     * between installations, you may wish to
     * {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * If the status does not correspond to a closed value, this value is
     * meaningless. Bugzilla allows the definition of custom workflows, so
     * maintaining a correct correspondence between resolution and status is the
     * responsibility of the caller.
     *
     * @param resolution A {@literal String} representing the resolution of this
     * bug.
     * @see {@link #clearResolution()}
     */
    public void setResolution(String resolution) {
        internalState.put(BugInfo.RESOLUTION, resolution);
    }

    /**
     * Removes any existing resolution for this {@link Bug}. Since a resolution
     * can only be applied to a closed bug, depending on the workflow defined by
     * the particular Bugzilla installation, changing a bug's status from closed
     * to open requires the caller also invoke this method. Because of this
     * customization capability, it is impossible to safely determine
     * automatically when to clear a set resolution.
     */
    public void clearResolution() {
        internalState.remove(BugInfo.RESOLUTION);
    }

    /**
     * Returns the operating system this bug was discovered to affect.
     *
     * @return A {@literal String} representing the name of the affected
     * operating system.
     */
    @Column
    public String getOperatingSystem() {
        return (String) internalState.get(BugInfo.OS);
    }

    /**
     * Sets the operating system this {@link Bug} was discovered to affect.
     * Since this field can be edited between installations, you may wish to
     * {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * @param os A {@literal String} representing the operating system name.
     */
    public void setOperatingSystem(String os) {
        internalState.put(BugInfo.OS, os);
    }

    /**
     * Returns the hardware platform this bug was discovered to affect. Since
     * this field can be edited between installations, you may wish to
     * {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * @return A {@literal String} representing the name of the affected
     * platform.
     */
    @Column
    public String getPlatform() {
        return (String) internalState.get(BugInfo.PLATFORM);
    }

    /**
     * Sets the platform affected by this {@link Bug}. Since this field can be
     * edited between installations, you may wish to
     * {@link com.j2bugzilla.rpc.GetLegalValues check its legal values}.
     *
     * @param platform A {@literal String} representing the platform name.
     */
    public void setPlatform(String platform) {
        internalState.put(BugInfo.PLATFORM, platform);
    }

    @Column
    public String getOpenDate() {
        return (String) internalState.get(BugInfo.OPEN_ON);
    }

    public void setOpenDate(String date) {
        internalState.put(BugInfo.OPEN_ON, date);
    }

    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getCreationTime() {
        return (Date) internalState.get(BugInfo.OPEN_ON);
    }

    public void setCreationTime(Date date) {
        internalState.put(BugInfo.OPEN_ON, date);
    }

    @Column
    public String getAssignee() {
        return (String) internalState.get(BugInfo.ASSIGNEE);
    }

    public void setAssignee(String assignee) {
        internalState.put(BugInfo.ASSIGNEE, assignee);
    }

    @Column
    public Double getEstimatedTime() {
        if (internalState.containsKey(BugInfo.ESTIMATED)) {
            return (Double) internalState.get("estimated_time");
        } else {
            return new Double(0);
        }
    }

    public void setEstimatedTime(Double estTime) {
        internalState.put("estimated_time", (estTime == null ? 0 : estTime));
    }

    @Column
    public Double getRemainingTime() {
        if (internalState.containsKey(BugInfo.REMAINING)) {
            return (Double) internalState.get(BugInfo.REMAINING);
        } else {
            return new Double(0);
        }
    }

    public void setRemainingTime(Double remTime) {

        internalState.put(BugInfo.REMAINING, (remTime == null ? 0 : remTime));

    }

    @Column
    public String getDeadline() {
        if (internalState.containsKey(BugInfo.DEADLINE)) {
            return (String) internalState.get(BugInfo.DEADLINE);
        } else {
            return "";
        }
    }

    public void setDeadline(String deadline) {

        internalState.put(BugInfo.DEADLINE, deadline);

    }

    @Column
    public String getPercentageComplete() {
        if (internalState.containsKey(BugInfo.COMPLETE)) {
            return (String) internalState.get(BugInfo.COMPLETE);
        } else {
            return "";
        }
    }

    public void setPercentageComplete(String percentage) {
        internalState.put(BugInfo.COMPLETE, percentage);

    }

    @Column
    public String getMilestone() {
        if (internalState.containsKey(BugInfo.MILESTONE)) {
            return (String) internalState.get(BugInfo.MILESTONE);
        } else {
            return "";
        }
    }

    public void setMilestone(String milestone) {

        internalState.put(BugInfo.MILESTONE, milestone);

    }

    /**
     * Returns the {@literal Set} of all {@link Flag Flags} recorded for this
     * {@link Bug}.
     *
     * @return A collection of {@literal Flags} recorded by the Bugzilla
     * installation against this {@literal Bug}.
     */
    @Transient
    public Set<Flag> getFlags() {
        Object[] flagObjs = (Object[]) internalState.get("flags");
        Set<Flag> flags = new HashSet<Flag>();
        for (Object obj : flagObjs) {
            @SuppressWarnings("unchecked")
            Map<String, Object> flag = (Map<String, Object>) obj;
            String name = (String) flag.get("name");
            String status = (String) flag.get(BugInfo.STATUS);
            Status s;
            switch (status) {
                case " ":
                    s = Status.UNSET;
                    break;
                case "?":
                    s = Status.UNKNOWN;
                    break;
                case "+":
                    s = Status.POSITIVE;
                    break;
                case "-":
                    s = Status.NEGATIVE;
                    break;
                default:
                    throw new IllegalStateException("Unknown flag state");
            }
            flags.add(new Flag(name, s));
        }
        return Collections.unmodifiableSet(flags);
    }

    @Transient
    public Map<Object, Object> getParameterMap() {
        Map<Object, Object> params = new HashMap<Object, Object>();
        for (String key : internalState.keySet()) {
            params.put(key, internalState.get(key));
        }
        return Collections.unmodifiableMap(params);
    }

    @Transient
    public Map<String, Object> getInternalState() {
        return internalState;
    }

}
