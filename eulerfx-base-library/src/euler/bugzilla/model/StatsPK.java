/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model;

import java.io.Serializable;

public class StatsPK implements Serializable {

    private Integer week;
    private Integer year;

    public StatsPK() {
    }

    public StatsPK(Integer week, Integer year) {
        this.week=week;
        this.year = year;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += ((week != null && year != null) ? week.hashCode() + year.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StatsPK)) {
            return false;
        }
        StatsPK other = (StatsPK) object;
        if (week != null && year != null) {
            return false;
        }
        if (week == other.week && year == other.year) {
            return true;
        }

        return false;
    }
}
