/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model;

import euler.bugzilla.j2bugzilla.base.Comment;
import static euler.bugzilla.model.BugInfo.CC;
import static euler.bugzilla.model.BugInfo.ID;
import euler.bugzilla.utils.ConcurrentDateFormatAccess;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
@Access(AccessType.PROPERTY)
public class CommentInfo implements Serializable {

    public static final String ID = "id";
    public static final String BUG_ID = "bug_id";
    public static final String ATTACHMENT_ID = "attachment_id";
    public static final String COUNT = "count";
    public static final String TEXT = "text";
    public static final String CREATOR = "creator";
    public static final String TIME = "time";

    private Map<String, Property> internalState = new LinkedHashMap<>();

    {
        Map<String, Property> aMap = new HashMap<>();
        aMap.put(ID, new SimpleIntegerProperty());
        aMap.put(BUG_ID, new SimpleIntegerProperty());
        aMap.put(ATTACHMENT_ID, new SimpleIntegerProperty());
        aMap.put(COUNT, new SimpleIntegerProperty());
        aMap.put(TEXT, new SimpleStringProperty());
        aMap.put(CREATOR, new SimpleStringProperty());
        aMap.put(TIME, new SimpleStringProperty());
        internalState = Collections.unmodifiableMap(aMap);
    }
    ;    
    
    private BugInfo bug;

    public CommentInfo() {
    }

    public CommentInfo(Comment comment) {
        ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess(BugInfo.DATEFORMAT);
        internalState.get(ID).setValue(comment.getID());
        internalState.get(BUG_ID).setValue(comment.getBugId());
        internalState.get(TEXT).setValue(comment.getText());
        internalState.get(CREATOR).setValue(comment.getAuthor());
        internalState.get(TIME).setValue(df.dateToString(comment.getTime()));
    }

    /**
     * @return The unique ID of this {@link CommentInfo}
     */
    @Id
    public int getID() {
        return (int) internalState.get(ID).getValue();
    }

    /**
     * @return The text content of this {@link CommentInfo}
     */
    @Column
    public String getText() {
        return (String) internalState.get(TEXT).getValue();
    }

    public int getId() {
        return (int) internalState.get(ID).getValue();
    }

    @Column
    public String getAuthor() {
        return (String) internalState.get(CREATOR).getValue();
    }

    @Column
    public String getTime() {
        return (String) internalState.get(TIME).getValue();
    }

    @ManyToOne
    public BugInfo getBug() {
        return bug;
    }

    public void setId(int id) {
        internalState.get(ID).setValue(id);
    }

    public void setText(String text) {
        internalState.get(TEXT).setValue(text);
    }

    public void setAuthor(String author) {
        internalState.get(CREATOR).setValue(author);
    }

    public void setTime(Date time) {
        ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess(BugInfo.DATEFORMAT);
        internalState.get(TIME).setValue(df.dateToString(time));
    }

    public void setBug(BugInfo bug) {
        this.bug = bug;
    }

}
