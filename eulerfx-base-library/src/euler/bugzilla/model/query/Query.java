/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model.query;

import euler.fx.tree.TreeElement;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.ServerInformation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.*;

/**
 *
 * @author agime
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Query.fetchQueryParams", query = "SELECT qp FROM Query q LEFT JOIN FETCH q.params qp LEFT JOIN FETCH qp.paramValues WHERE q.name =:queryName"),
    @NamedQuery(name = "Query.fetchBugs", query = "SELECT qb FROM Query q LEFT JOIN FETCH q.bugs qb WHERE q =:query"),
    @NamedQuery(name = "Query.countBugs", query = "SELECT COUNT(qb) FROM Query q LEFT JOIN FETCH q.bugs qb WHERE q.id =:queryId"),
    @NamedQuery(name = "Query.fetchBugsbyName", query = "SELECT qb FROM Query q LEFT JOIN FETCH q.bugs qb WHERE q.name =:queryName"),
    @NamedQuery(name = "Query.getAll", query = "SELECT q FROM Query q"),
    @NamedQuery(name = "Query.byName", query = "SELECT q FROM Query q WHERE q.name =:queryName"),
    @NamedQuery(name = "Query.byFolder", query = "SELECT q FROM Query q WHERE q.folder =:folder"),
    @NamedQuery(name = "Query.getServer", query = "SELECT q.server FROM Query q WHERE q =:query")
})
public class Query implements TreeElement, Serializable{

    private Long id;
    private String name;
    private List<QueryParameters> params = new ArrayList<>();
    private List<BugInfo> bugs = new ArrayList<>();
    private QueryFolder folder;
    private int nbugs;
        
    private StringProperty displayName = new SimpleStringProperty();    
    private ServerInformation server;
    
    @Id
    @GeneratedValue
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public QueryFolder getFolder() {
        return folder;
    }

    public void setFolder(QueryFolder folder) {
        this.folder = folder;
    }

    @ManyToOne
    public ServerInformation getServer() {
        return server;
    }

    public void setServer(ServerInformation server) {
        this.server = server;
    }

    @Column
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {        
        this.name = name;
    }

    @OneToMany
    public List<QueryParameters> getParams() {
        return params;
    }

    public void setParams(List<QueryParameters> params) {
        this.params = params;
    }

    @OneToMany
    public List<BugInfo> getBugs() {
        return bugs;
    }

    public void setBugs(List<BugInfo> bugs) {
        this.bugs = bugs;
        this.nbugs = bugs.size();
        StringBuilder sb = new StringBuilder(name);
        sb.append(" (");
        sb.append(nbugs);
        sb.append(")");
        displayName.set(sb.toString());        
    }

    @Column
    public int getNbugs() {
        return nbugs;
    }

    public void setNbugs(int nbugs) {
        this.nbugs = nbugs;
    }
    
    @Column
    @Override
    public String getDisplayName() {
        return displayName.get();
    }

    public void setDisplayName(String displayName) {
        this.displayName.set(displayName);
    }

    @Override
    public TreeTypeEnum getType() {
        return TreeTypeEnum.QUERY;
    }
    
    @Override
    public StringProperty displayNameProperty(){
        return displayName;
    }
    
    public Map<SearchLimiter, List<String>> getParamMap() {
        Map<SearchLimiter, List<String>> p = new HashMap<>();
        for (QueryParameters qp : getParams()) {
            p.put(qp.getLimiter(), qp.getValues());
        }
        return p;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Query other = (Query) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }



}
