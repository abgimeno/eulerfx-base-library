/*
* To change this template, choose Tools | Templates 
* and open the template in the editor.
*/
package euler.bugzilla.model.query;

import euler.bugzilla.model.ProductInfo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author root
 * 
 */
@Entity
@NamedQueries({
  @NamedQuery(name="LV.fetchLegalValues", query="SELECT DISTINCT lv.legalValues FROM LegalValue lv WHERE lv.field =:field"),  
  @NamedQuery(name="LV.fetchLegalValues4Product", query="SELECT DISTINCT lv.legalValues FROM LegalValue lv WHERE lv.field =:field AND lv.product=:product"),
  @NamedQuery(name="LV.fetchLegalValuesInProducts", query="SELECT DISTINCT lv.legalValues FROM LegalValue lv WHERE lv.field =:field AND lv.product IN :products"),

})
public class LegalValue implements Serializable {
    
    @Id
    @GeneratedValue
    private Long id;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable=false)
    private Fields field;

    @ElementCollection
    @CollectionTable(name ="LEGALVALUES")
    private List<String> legalValues = new ArrayList<>();            
    
    @OneToOne
    private ProductInfo product;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Fields getField() {
        return field;
    }

    public void setField(Fields field) {
        this.field = field;
    }

    public List<String> getLegalValues() {
        return legalValues;
    }

    public void setLegalValues(List<String> legalValues) {
        this.legalValues = legalValues; 
    }

    public ProductInfo getProduct() {
        return product;
    }

    public void setProduct(ProductInfo product) {
        this.product = product;
    }
    
}
