/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model.query;

import euler.bugzilla.model.ServerInformation;

/**
 *
 * @author agime
 */
public enum TreeTypeEnum {

        SERVER(ServerInformation.class),
        FOLDER(QueryFolder.class),
        QUERY(Query.class);
        
        private final Class type;

        /**
         * Creates a new {@link SearchLimiter} with the designated name
         *
         * @param name The name Bugzilla expects for this search limiter
         */
        TreeTypeEnum(Class type) {
            this.type = type;
        }

        /**
         * Get the name Bugzilla expects for this search limiter
         *
         * @return A <code>String</code> representing the search limiter
         */
        public Class getType() {
            return this.type;
        }    
}
