/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model.query;

import euler.fx.tree.TreeElement;
import euler.bugzilla.model.ServerInformation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author agime
 */
@Entity
@NamedQueries({
    @NamedQuery(name="QF.getAllFetchQueries", query="SELECT qf FROM QueryFolder qf"),
    @NamedQuery(name="QF.fetchQueries", query="SELECT qf FROM QueryFolder qf JOIN FETCH qf.queries qfe WHERE qf.id =:id")
})
public class QueryFolder implements TreeElement, Serializable {
    
    private Long id;
    private String name;
    private List<Query> queries = new ArrayList<>();
    private ServerInformation bc;
    private final StringProperty displayName = new SimpleStringProperty();

    @Id
    @GeneratedValue
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column
    @Override
    public String getName() {
        return displayName.get();
    }

    @Override
    public void setName(String name) {
        this.name = name;
        displayName.set(name);
    }

    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    public List<Query> getQueries() {
        return queries;
    }

    public void setQueries(List<Query> queries) {
        this.queries = queries;
    }

    @ManyToOne
    public ServerInformation getBc() {
        return bc;
    }

    public void setBc(ServerInformation bc) {
        this.bc = bc;
    }

    @Override
    public TreeTypeEnum getType() {
        return TreeTypeEnum.FOLDER;
    }

    @Column
    @Override
    public String getDisplayName() {
        return displayName.get();
    }
    public void setDisplayName(String name) {
        this.displayName.set(name);
    }
    @Override
    public StringProperty displayNameProperty() {
        return displayName;
    }  
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QueryFolder other = (QueryFolder) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }    
}
