/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model.query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author root
 */
public enum Fields {
        
        COMPONENT("component"),
        VERSION("version"),
        REP_PLATFORM("rep_platform"),
        OP_SYS("op_sys"),
        PRIORITY("priority"),
        SEVERITY("bug_severity"),
        STATUS("bug_status"),
        RESOLUTION("resolution"),
        MILESTONE("target_milestone");
       // ASIGNEE("assigned_to"),
       // CREATOR("creator"),
        //CC("cc");
        
        //each new field must be added to getNames() method below
        private final String internalName;

        Fields(String internalName) {
            this.internalName = internalName;
        }

        public String getInternalName() {
            return internalName;
        }

        public static List<String> getNames() {
            List<String> names = new ArrayList<>();
            names.add(COMPONENT.getInternalName());
            names.add(VERSION.getInternalName());
            names.add(REP_PLATFORM.getInternalName());
            names.add(OP_SYS.getInternalName());
            names.add(PRIORITY.getInternalName());
            names.add(SEVERITY.getInternalName());
            names.add(STATUS.getInternalName());
            names.add(RESOLUTION.getInternalName());
            names.add(MILESTONE.getInternalName());
            //names.add(ASIGNEE.getInternalName());
            //names.add(CREATOR.getInternalName());
            //names.add(CC.getInternalName());
            names = Collections.unmodifiableList(names);
            return names;
        }

        public boolean dependsOnProduct() {
            return this.internalName.equals(COMPONENT.getInternalName())
                    || this.internalName.equals(VERSION.getInternalName())
                    || this.internalName.equals(MILESTONE.getInternalName());
        }
    }
