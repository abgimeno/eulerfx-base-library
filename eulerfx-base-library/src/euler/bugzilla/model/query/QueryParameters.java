/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * The {@code SearchQuery} class encapsulates a query against the bug collection
 * on a given Bugzilla database. It consists of a limiter to apply and the value
 * for that limiter. For example, a valid {@code SearchQuery} might consist of
 * the limiter {@link SearchLimiter#PRODUCT "Product"} and the query
 * {@code "J2Bugzilla"}.
 *
 * When a {@code SearchQuery} is applied within the {@link BugSearch} class, it
 * is joined with the other queries in a logical AND. That is, bugs will be
 * returned that match all the criteria, not any of them.
 *
 * @author Tom
 *
 */
@Entity
public class QueryParameters implements Serializable{

    @Id
    @GeneratedValue
    private Long id;
    
    @Enumerated(EnumType.STRING)
    private SearchLimiter limiter;
    
    @ElementCollection
    @CollectionTable(name ="QUERYPARAM2VALUES")
    private List<String> paramValues = new ArrayList<>();

    public QueryParameters() {
    }

    /**
     * Creates a new {@link SearchQuery} to filter the bug database through.
     *
     * @ limiter A {@link SearchLimiter} enum.
     * @param query A {@code String} to filter with. The whole string will be
     * matched, or not at all -- Bugzilla does not perform substring matching.
     */
    public QueryParameters(SearchLimiter limiter, String query) {
        this.limiter = limiter;
        this.paramValues.add(query);
    }

    public QueryParameters(SearchLimiter limiter, List<String> query) {
        this.limiter = limiter;
        this.paramValues.addAll(query);
    }

    public void setLimiter(SearchLimiter limiter) {
        this.limiter = limiter;
    }

    public List<String> getValues() {
        return paramValues;
    }

    /**
     * Returns the {@link SearchLimiter} to apply to this query.
     *
     * @return A facet of a bug to search against.
     */
    public SearchLimiter getLimiter() {
        return limiter;
    }

    /**
     * Returns the value of the specified query.
     *
     * @return A {@code String} to query for within the specified limiter.
     */
    public List<String> getQuery() {
        return paramValues;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
