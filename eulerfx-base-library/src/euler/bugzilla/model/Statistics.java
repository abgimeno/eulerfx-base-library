/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;

/**
 *
 * @author agime
 */
@Entity
@IdClass(StatsPK.class)
@NamedQueries({
    @NamedQuery(name="Statistics.getAll", query="SELECT s FROM Statistics s")
})
public class Statistics implements Serializable {

    public Statistics() {
    }

    private static final long serialVersionUID = 1L;
    
    @Id Integer week;
    @Id Integer year;
    
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date snapshotDate;
    
    @Column
    private Integer resolved;
    
    @Column
    private Integer tested;
    
    @Column
    private Integer firstPrioOpen;
    
    @Column
    private Integer secondPrioOpen;
    
    @Column
    private Integer propagation;
    
    @Column
    private Integer opened;

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer statsWeek) {
        this.week = statsWeek;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer statsYear) {
        this.year = statsYear;
    }


    public Date getSnapshotDate() {
        return snapshotDate;
    }

    public void setSnapshotDate(Date snapshotDate) {
        this.snapshotDate = snapshotDate;
    }

    public Integer getResolved() {
        return resolved;
    }

    public void setResolved(Integer resolved) {
        this.resolved = resolved;
    }

    public Integer getTested() {
        return tested;
    }

    public void setTested(Integer tested) {
        this.tested = tested;
    }

    public Integer getFirstPrioOpen() {
        return firstPrioOpen;
    }

    public void setFirstPrioOpen(Integer firstPrioOpen) {
        this.firstPrioOpen = firstPrioOpen;
    }

    public Integer getSecondPrioOpen() {
        return secondPrioOpen;
    }

    public void setSecondPrioOpen(Integer secondPrioOpen) {
        this.secondPrioOpen = secondPrioOpen;
    }

    public Integer getPropagation() {
        return propagation;
    }

    public void setPropagation(Integer propagation) {
        this.propagation = propagation;
    }

    public Integer getOpened() {
        return opened;
    }

    public void setOpened(Integer opened) {
        this.opened = opened;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (week != null ? week.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statistics)) {
            return false;
        }
        Statistics other = (Statistics) object;
        if ((this.week == null && other.week != null) || (this.week != null && !this.week.equals(other.week))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "epps.bugzilla.monitoring.model.Statistics[ id=" + week + " ]";
    }
}
