/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model;

import euler.bugzilla.model.query.TreeTypeEnum;
import euler.fx.tree.TreeElement;
import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author agime
 */
@Entity
@NamedQueries({
    @NamedQuery(name="SI.getAll", query="SELECT si FROM ServerInformation  si"),
    @NamedQuery(name="SI.getByUsernameAndPass", query="SELECT si FROM ServerInformation  si WHERE si.username =:username AND si.password =:password"), 
    @NamedQuery(name="SI.getByURL", query="SELECT si FROM ServerInformation  si WHERE si.url =:url"), 
})
public class ServerInformation implements TreeElement, Serializable {
    
    private static final long serialVersionUID = 1L;
    private Long id;
    private String username;
    private String password;
    private String url;
    private String name;
    private StringProperty displayName = new SimpleStringProperty();
            
    public ServerInformation() {
    }

    public ServerInformation(String username, String password, String bugzillaUrl) {
        this.username = username;
        this.password = password;
        this.url = bugzillaUrl;
        this.name = bugzillaUrl;
    }   
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column
    public String getUrl() {
        return url;
    }

    public void setUrl(String bugzillaUrl) {
        this.url = bugzillaUrl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServerInformation)) {
            return false;
        }
        ServerInformation other = (ServerInformation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "epps.bugzilla.monitoring.model.ServerInformation[ id=" + id + " ]";
    }

    @Column
    @Override
    public String getName() {
        return name;
    }

    @Override
    public TreeTypeEnum getType() {
       return TreeTypeEnum.SERVER;
    }

    @Override
    public void setName(String name) {
        this.name= name;
        displayName.set(name);
    }

    @Column
    @Override
    public String getDisplayName() {
        return displayName.get();
    }
    
    public void setDisplayName(String name) {
        this.displayName.set(name);
    }
    
    @Override
    public StringProperty displayNameProperty() {
        return displayName;
    }
    
}
