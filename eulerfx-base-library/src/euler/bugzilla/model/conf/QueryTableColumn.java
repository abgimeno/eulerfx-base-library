/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.model.conf;

import euler.bugzilla.model.query.Query;
import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;


/**
 *
 * @author agime
 */

@Entity
@Access(AccessType.PROPERTY)
@NamedQueries({
    @NamedQuery(name="QueryTableColumn.getByName", query="SELECT qtc FROM QueryTableColumn qtc WHERE qtc.name =:name")
})
public class QueryTableColumn implements Serializable{
    
    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final DoubleProperty width = new SimpleDoubleProperty(0.0);
    private final BooleanProperty visible = new SimpleBooleanProperty(false);
    private final IntegerProperty position = new SimpleIntegerProperty();   
    
    
    public QueryTableColumn() {
    }

    public QueryTableColumn(String columnName) {
        this.name.set(columnName);
    }

    @Id
    @GeneratedValue
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id.set(id);
    }
    
    @Column
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }
    
    @Column
    public Double getWidth() {
        return width.get();
    }

    public void setWidth(Double width) {
        this.width.set(width);
    }
    
    @Column(name="VISIBLE")
    public Boolean getVisible(){
        return visible.get();
    }
    
    public Boolean isVisible(){
        return visible.get();        
    }
    
    public void setVisible(Boolean visible){
        this.visible.set(visible);
    }
    
    @Transient
    public BooleanProperty getVisibleProperty(){
        return visible;
    }


    @Column
    public Integer getPosition(){
    return position.get();
    }
    public void setPosition(Integer pos){
    position.set(pos);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QueryTableColumn other = (QueryTableColumn) obj;
        Boolean equality = Objects.equals(this.id.get(), other.id.get()) && 
               Objects.equals(this.name.get(), other.name.get());
        //Logger.getLogger(this.getClass()).trace(this.id.get() +" is equals to "+other.id.get());
        //Logger.getLogger(this.getClass()).trace(this.name.get() +" is equals to "+other.name.get());
        //Logger.getLogger(this.getClass()).debug("Equality is " + equality);
        return Objects.equals(this.id.get(), other.id.get()) && 
               Objects.equals(this.name.get(), other.name.get());    
    }
    
}
