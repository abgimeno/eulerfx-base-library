/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model.conf;

import java.util.Comparator;
import java.util.Objects;

/**
 *
 * @author Abraham Gimeno
 */
public class PositionComparator implements Comparator<QueryTableColumn> {


        @Override
        public int compare(QueryTableColumn o1, QueryTableColumn o2) {
            return Integer.compare(o1.getPosition(), o2.getPosition());
        }
    
}
