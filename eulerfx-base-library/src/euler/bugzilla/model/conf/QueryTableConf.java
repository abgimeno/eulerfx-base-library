/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model.conf;

import euler.bugzilla.model.query.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author Abraham Gimeno
 */
@Entity
@Access(AccessType.FIELD)
@NamedQueries({
    @NamedQuery(name="QueryTableConf.getConfiguration", query="SELECT DISTINCT qtconf FROM QueryTableConf qtconf LEFT JOIN FETCH qtconf.columns WHERE qtconf.query=:query")
})
public class QueryTableConf implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }
    
    @ManyToOne
    @JoinColumn(unique = true)
    private Query query;

    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinTable
    private List<QueryTableColumn> columns = new ArrayList<>();
    public void setId(Long id) {
        this.id = id;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public List<QueryTableColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<QueryTableColumn> columns) {
        this.columns = columns;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof QueryTableConf)) {
            return false;
        }
        QueryTableConf other = (QueryTableConf) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "euler.bugzilla.model.conf.TableViewConf[ id=" + id + " ]";
    }
    
}
