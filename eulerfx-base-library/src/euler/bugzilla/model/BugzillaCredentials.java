/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author agime
 */
@Entity
@NamedQueries({
    @NamedQuery(name="BC.getAll", query="SELECT bc FROM BugzillaCredentials  bc"),
    @NamedQuery(name="BC.getByUsernameAndPass", query="SELECT bc FROM BugzillaCredentials  bc WHERE bc.username =:username AND bc.password =:password")
})
public class BugzillaCredentials implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column
    private String username;
    
    @Column 
    private String password;
    
    @Column
    private String url;

    public BugzillaCredentials() {
    }

    public BugzillaCredentials(String username, String password, String bugzillaUrl) {
        this.username = username;
        this.password = password;
        this.url = bugzillaUrl;
    }

    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String bugzillaUrl) {
        this.url = bugzillaUrl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BugzillaCredentials)) {
            return false;
        }
        BugzillaCredentials other = (BugzillaCredentials) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "epps.bugzilla.monitoring.model.BugzillaCredentials[ id=" + id + " ]";
    }
    
}
