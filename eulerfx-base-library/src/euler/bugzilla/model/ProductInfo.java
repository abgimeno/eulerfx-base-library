/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.model;


import euler.bugzilla.j2bugzilla.base.Product;
import java.io.Serializable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author agime
 */
@Entity
@Access(AccessType.PROPERTY)
@NamedQueries({
    @NamedQuery(name="PI.countProducts", query="SELECT COUNT(pi) FROM ProductInfo pi"), 
    @NamedQuery(name="PI.getAll", query="SELECT pi FROM ProductInfo pi"),
    @NamedQuery(name="PI.getActive", query="SELECT pi FROM ProductInfo pi WHERE pi.selected = TRUE"),
    @NamedQuery(name="PI.getByName", query="SELECT pi FROM ProductInfo pi WHERE pi.name =:name")
})
public class ProductInfo implements Serializable {

    private final IntegerProperty id = new SimpleIntegerProperty();
    private final StringProperty name = new SimpleStringProperty();
    private final StringProperty description = new SimpleStringProperty();
    private final BooleanProperty selected = new SimpleBooleanProperty(false);

    public ProductInfo() {
        
    }

    public ProductInfo(Product p) {
        this();
        this.id.set(p.getID());
        this.name.set(p.getName());
        this.description.set(p.getDescription());        
    }

    @Id
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id.set(id);
    }

    @Column(unique= true)
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @Column(length=100000)
    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    @Column
    public Boolean getSelected() {
        return selected.get();
    }

    public void setSelected(Boolean selected) {
        this.selected.set(selected);
    }
    
    
    public IntegerProperty idProperty() {
        return id;
    }
   
    public StringProperty nameProperty() {
        return name;
    }
    
    public StringProperty descriptionProperty() {
        return description;
    }
    
    public BooleanProperty selectedProperty() {
        return selected;
    }
    
}
