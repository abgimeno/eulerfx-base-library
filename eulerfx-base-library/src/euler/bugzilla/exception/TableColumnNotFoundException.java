/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package euler.bugzilla.exception;

/**
 *
 * @author agime
 */
public class TableColumnNotFoundException extends Exception {

    public TableColumnNotFoundException(String message) {
        super(message);
    }
    
}
