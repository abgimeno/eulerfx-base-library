/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.utils;

import euler.bugzilla.exception.LegalValuesMissingException;
import euler.bugzilla.beans.query.LegalValueBean;
import euler.bugzilla.beans.user.UserBean;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.j2bugzilla.base.ConnectionException;
import euler.bugzilla.j2bugzilla.base.Product;
import euler.bugzilla.j2bugzilla.enums.UserInfoParams;
import euler.bugzilla.j2bugzilla.rpc.LogIn;
import euler.bugzilla.model.BugzillaUser;
import euler.bugzilla.j2bugzilla.user.UserInfo;
import euler.bugzilla.model.ProductInfo;
import euler.bugzilla.model.ServerInformation;
import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.SearchLimiter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class LegalValuesUtils {

    public static ListProperty getItemsForField(String persitenceUnit, Fields field) {
        return getItemsForField(persitenceUnit, field, null);
    }

    public static ListProperty getItemsForField(String persitenceUnit, Fields field, ProductInfo product) {
        try {
            final LegalValueBean lvs = new LegalValueBean(persitenceUnit);
            return product == null ? LegalValuesUtils.retrieveLegalValuesForField(field, lvs)
                    : LegalValuesUtils.retrieveLegalValuesForField(field, product, lvs);
        } catch (IOException | LegalValuesMissingException ex) {
            Logger.getLogger(LegalValuesUtils.class).error(null, ex);
            return null;
        }
    }

    private static ListProperty retrieveLegalValuesForField(Fields field, final LegalValueBean lvs) throws LegalValuesMissingException {
        return retrieveLegalValuesForField(field, null, lvs);
    }

    
    
    private static ListProperty retrieveLegalValuesForField(Fields field, ProductInfo product, final LegalValueBean lvs) throws LegalValuesMissingException {
        Logger.getLogger(LegalValuesUtils.class).debug("Retrieving values for " + field.getInternalName());
        ListProperty lp;
        if (areLegalValuesStoredOnDB(field, lvs)) {
            lp = new SimpleListProperty();
            Logger.getLogger(LegalValuesUtils.class).info("Retrieving stored legal values for " + field.getInternalName());
            ObservableList items = new SimpleListProperty(FXCollections.observableArrayList());
            items.addAll((product == null) ? lvs.getLegalValues(field) : lvs.getLegalValues(field, product));
            lp.setValue(items);
        } else {
            throw new LegalValuesMissingException("No legal values found on DB, please run configuration script");
        }

        return lp;
    }


    private static boolean areLegalValuesStoredOnDB(Fields field, LegalValueBean lvs) {
        final List<String> legalValues = lvs.getLegalValues(field);
        return !(legalValues == null || legalValues.isEmpty());
    }

    public static List<String> getProductNames(List<Product> products) {
        List<String> names = new ArrayList<>();
        for (Product p : products) {
            names.add(p.getName());
        }
        return names;
    }

    public static List<BugzillaUser> getUserList(String persistenceUnit, ServerInformation bc) throws BugzillaException {
        List<BugzillaUser> names = new ArrayList<>();
        UserBean userBean = null;
        try {
            if (usersExistInDB()) {
                userBean = new UserBean(persistenceUnit);
                names = userBean.getAllUsers(bc);
            } else {
                /*The below code retrieves list of all users for
                 * autocompleting search field, but with lot of users 
                 * like on mozilla.org there are performance issues
                 * throw new LegalValuesMissingException("No legal values found on DB, please run configuration script");
                 **/

                LogIn logIn = new LogIn(bc.getUsername(), bc.getPassword());
                BugzillaConnector conn = new BugzillaConnector();
                conn.connectTo(bc.getUrl());
                conn.executeMethod(logIn);
                Map<UserInfoParams, List<String>> parameters = new HashMap<>();
                List<String> keys = new ArrayList<>();
                keys.add("*");
                parameters.put(UserInfoParams.MATCH, keys);
                keys = new ArrayList<>();
                keys.add("name");
                keys.add("real_name");
                keys.add("email");
                parameters.put(UserInfoParams.INCLUDE, keys);
                UserInfo method = new UserInfo(parameters);
                conn.executeMethod(method);
                for (BugzillaUser bu : method.getResults()) {
                    names.add(bu);
                }
            }

        } catch (IOException | ConnectionException ex) {
            Logger.getLogger(LegalValuesUtils.class).error(null, ex);
        }
        return names;
    }
    /* the below are the parameters filtered by product on bugzilla, 
     * since user selects a subset of product values not associated with the 
     * products selected by the user are left out
     */

    public static boolean filterResults(Fields f) {
        return filterResults(f.getInternalName());
    }
    public static boolean filterResults(SearchLimiter s) {
        return filterResults(s.getName());
    }    
    private static boolean filterResults(String field) {
        return (field.equals("component")
                || field.equals("version")
                || field.equals("target_milestone"));
    }
    private static boolean usersExistInDB() {
        Logger.getLogger(LegalValuesUtils.class).debug(LegalValuesUtils.class + " static boolean usersExistInDB()  HARCODED RETURN = true");
        return true;
    }

    public static List<String> getUserList(ServerInformation bc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
