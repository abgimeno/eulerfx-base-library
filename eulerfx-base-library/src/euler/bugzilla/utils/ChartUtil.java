/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.utils;

import euler.bugzilla.model.Statistics;
import java.util.Calendar;
import java.util.List;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

/**
 *
 * @author agime
 */
public class ChartUtil {

    public static LineChart<String, Number> createStatusChart(List<Statistics> stats) {
        if (stats.isEmpty()) {
            return null;
        }
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<String, Number> lc = new LineChart<>(xAxis, yAxis);


        // setup chart
        lc.setMaxSize(850, 850);
        lc.setMinSize(650, 650);
        lc.setTitle("Overview of open Issues");
        xAxis.setLabel("Date");
        yAxis.setLabel("Bug Number");
        // add starting data
        XYChart.Series<String, Number> n1stPrio = new XYChart.Series<>();
        XYChart.Series<String, Number> n2dPrio = new XYChart.Series<>();
        XYChart.Series<String, Number> nResolved = new XYChart.Series<>();
        XYChart.Series<String, Number> nTested = new XYChart.Series<>();
        XYChart.Series<String, Number> nPropag = new XYChart.Series<>();
        XYChart.Series<String, Number> nOpen = new XYChart.Series<>();

        n1stPrio.setName("First Priority Bugs Open");
        n2dPrio.setName("Second Priority Bugs Open");
        nPropag.setName("Propagation Bugs Open");
        nResolved.setName("Bugs Resolved");
        nTested.setName("Bugs Tested");
        nOpen.setName("New Bugs Added");
        ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess("dd/MM/YYY"); 
        for (Statistics s : stats) {
            String date = df.dateToString(getMonday(s).getTime());
            n1stPrio.getData().add(new XYChart.Data<String, Number>(date, s.getFirstPrioOpen()));
            n2dPrio.getData().add(new XYChart.Data<String, Number>(date, s.getSecondPrioOpen()));
            nResolved.getData().add(new XYChart.Data<String, Number>(date, s.getResolved()));
            nTested.getData().add(new XYChart.Data<String, Number>(date, s.getTested()));
            nPropag.getData().add(new XYChart.Data<String, Number>(date, s.getPropagation()));
            nOpen.getData().add(new XYChart.Data<String, Number>(date, s.getOpened()));
        }

        lc.getData().add(n1stPrio);
        lc.getData().add(n2dPrio);
        lc.getData().add(nOpen);
        //lc.getData().add(nResolved);
        //lc.getData().add(nTested);
        //lc.getData().add(nPropag);

        return lc;
    }

    public static LineChart<String, Number> createActivityChart(List<Statistics> stats) {
        if (stats.isEmpty()) {
            return null;
        }
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<String, Number> lc = new LineChart<>(xAxis, yAxis);


        // setup chart
        lc.setMaxSize(850, 850);
        lc.setMinSize(650, 650);
        lc.setTitle("Team activity");
        xAxis.setLabel("Date");
        yAxis.setLabel("Bug Number");
        // add starting data
        XYChart.Series<String, Number> nResolved = new XYChart.Series<>();
        XYChart.Series<String, Number> nTested = new XYChart.Series<>();
        XYChart.Series<String, Number> nPropag = new XYChart.Series<>();
        
        nPropag.setName("Propagation Bugs Open");
        nResolved.setName("Bugs Resolved");
        nTested.setName("Bugs Tested");
        ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess("dd/MM/YYY"); 
        for (Statistics s : stats) {            
            String date = df.dateToString(getMonday(s).getTime());
            nResolved.getData().add(new XYChart.Data<String, Number>(date, s.getResolved()));
            nTested.getData().add(new XYChart.Data<String, Number>(date, s.getTested()));
            nPropag.getData().add(new XYChart.Data<String, Number>(date, s.getPropagation()));
        }
        
        lc.getData().add(nResolved);
        lc.getData().add(nTested);
        //lc.getData().add(nPropag);

        return lc;
    }    
    
    private static Calendar getMonday(Statistics s) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_YEAR, s.getWeek());
        cal.set(Calendar.YEAR, s.getYear());
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return cal;
    }
}
