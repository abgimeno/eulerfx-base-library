/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.utils;

import euler.bugzilla.model.BugInfo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import javafx.scene.control.TableView;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import euler.bugzilla.dialogfx.DialogFX;
import euler.bugzilla.dialogfx.DialogFX.Type;
import javafx.scene.control.TableColumn;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.PrintSetup;

/**
 *
 * @author agime
 */
public class BugInfoExportUtilities {

    private static ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess("yyyy-MM-dd");
    private static EulerExceptionUtils guiLogger = EulerExceptionUtils.getInstance();

    public static DialogFX exportToCSV(List<? extends TableView<BugInfo>> tables) throws Exception {
        DialogFX dialog = new DialogFX(Type.ERROR);
        Writer writer = null;
        File file = new File("bugs.csv");
        writer = new BufferedWriter(new FileWriter(file));
        for (TableView<BugInfo> tv : tables) {
            for (BugInfo bi : tv.getItems()) {
                //Logger.getLogger(J2BugService.class.getName()).log(Level.INFO, bi.toString());
                writer.write(bi.toString());
            }
        }
        setCSVDialogSuccess(dialog, file);
        writer.close();
        return dialog;

    }

    public static DialogFX exportToExcel(String filename, List<? extends TableView<BugInfo>> tables) {
        DialogFX dialog = new DialogFX(Type.ERROR);
        File f;
        FileOutputStream out;
        Map<String, CellStyle> styles;
        try {

            //setExcelDialogError();
            StringBuilder filenameBuilder = new StringBuilder(filename);
            filenameBuilder.append(".xls");
            f = new File(filenameBuilder.toString());
            out = new FileOutputStream(f);
            // create a new workbook
            Workbook workbook = new HSSFWorkbook();
            styles = StyleLibrary.createStyles(workbook);            
            for (int i = 0; i < tables.size(); i++) {
                StringBuilder sb = new StringBuilder(tables.get(i).getId());
                sb.append("(");
                sb.append(tables.get(i).getItems().size());
                sb.append(")");
                String sheetName = sb.toString();
                Sheet sheet = workbook.createSheet(sheetName);
                sheet.getHeader().setCenter(sheetName);
                // if the table is empty  skip creating the cells
                if(!tables.get(i).getItems().isEmpty()){ 
                    createCells(sheet, tables.get(i), styles);
                /*
                 * the below is hard coded to meet particular need  ;(
                 * (I dont want to print all columns but only till 8th)
                 * TO-DO: investigate how to dinamically retrieve a particular column
                 * from table view
                 */
                workbook.setPrintArea(i, 0, 8, 0, tables.get(i).getItems().size() + 1);
                }
            }
            workbook.write(out);
            out.close();            
            return setExcelDialogSuccess(dialog, f);
        } catch (IOException | NullPointerException ex) {
            Logger.getLogger(BugInfoExportUtilities.class).error(Level.ERROR, ex);
            dialog.setTitle(ex.getMessage());
            dialog.setMessage(guiLogger.printTraceToString(ex));
        }
        return dialog;

    }

    private static void createCells(Sheet sheet, TableView<BugInfo> table, Map<String, CellStyle> styles) throws NullPointerException {        
        Row row = sheet.createRow(0);
        configurePrintSetup(sheet);
        createHeaderCells(row, table, styles);
        //setAutoFilter(sheet, table);
        createContentCells(sheet, row, table, styles);

    }

    private static void configurePrintSetup(Sheet sheet) throws NullPointerException {
        PrintSetup ps = sheet.getPrintSetup();
        ps.setPaperSize(PrintSetup.A4_PAPERSIZE);
        ps.setLandscape(true);
        sheet.setAutobreaks(true);
        ps.setFitHeight((short) 1);
        ps.setFitWidth((short) 1);
    }

    private static void createHeaderCells(Row row, TableView<BugInfo> table, Map<String, CellStyle> styles) throws NullPointerException {
        Cell cell;
        short i = 0;
        for(TableColumn col : table.getColumns()){
            if(col.isVisible()){
                cell = row.createCell(i);
                cell.setCellValue((String) col.getText());
                cell.setCellStyle(styles.get("header"));
                i++;
            }
        }
    }

    private static void setAutoFilter(Sheet sheet, TableView<BugInfo> table) throws NullPointerException {
        StringBuilder sb = new StringBuilder("A1:");
        sb.append(Character.toString((char) (65 + table.getColumns().size() - 1)));
        sb.append("1");
        sheet.setAutoFilter(org.apache.poi.ss.util.CellRangeAddress.valueOf(sb.toString()));
    }

    private static void createContentCells(Sheet sheet, Row row, TableView<BugInfo> table, Map<String, CellStyle> styles) {
        
        Cell cell;
        for (short i = 0; i < table.getItems().size(); i++) {
            row = sheet.createRow(i + (short) 1);            
            BugInfo bi = table.getItems().get(i);
            int j = 0;  
            for (TableColumn col : table.getColumns()) {                             
                if (col.isVisible()) {                    
                        cell = row.createCell(j);
                        cell.setCellValue((String) col.getCellData(bi));
                        String style = bi.getOpenedThisWeek() ? "opened_this_week" : "cell_normal";
                        cell.setCellStyle(styles.get(style));
                        sheet.autoSizeColumn(j);
                        j++;                   
                }
            }
        }
        
        /*for (short i = 0; i < table.getItems().size(); i++) {
            BugInfo bi = table.getItems().get(i);
            row = sheet.createRow(i + (short) 1);
            for (short j = 0; j < bi.getInternalState().values().size(); j++) {
                Property prop = (Property) bi.getInternalState().values().toArray()[j];
                cell = row.createCell(j);
                cell.setCellValue(prop.getValue()==null? "":prop.getValue().toString());
                String style = bi.getOpenedThisWeek() ? "opened_this_week" : "cell_normal";
                cell.setCellStyle(styles.get(style));
                sheet.autoSizeColumn(j);
            }
        }*/
    }


    private static DialogFX setExcelDialogSuccess(DialogFX dialog, File f) {
        dialog = new DialogFX(Type.ACCEPT);
        dialog.setTitleText("Export to Excel");
        dialog.setMessage("File created under \"" + f.getAbsolutePath() + "\"");
        return dialog;
    }

    private static DialogFX setCSVDialogSuccess(DialogFX dialog, File file) {
        dialog = new DialogFX(Type.ACCEPT);
        dialog.setTitleText("Export to CSV");
        dialog.setMessage("File created under \"" + file.getAbsolutePath() + "\" ");
        return dialog;
    }


}
