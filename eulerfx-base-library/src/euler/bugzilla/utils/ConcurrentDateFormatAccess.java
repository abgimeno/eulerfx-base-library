/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConcurrentDateFormatAccess {
    
    private String format;

    public ConcurrentDateFormatAccess(String format) {
        this.format = format;
    }
    
    
    private ThreadLocal<DateFormat> df = new ThreadLocal<DateFormat>() {
        @Override
        public DateFormat get() {
            return super.get();
        }

        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat(format);
        }

        @Override
        public void remove() {
            super.remove();
        }

        @Override
        public void set(DateFormat value) {
            super.set(value);
        }
    };

    public Date stringToDate(String dateString) throws ParseException {
        return df.get().parse(dateString);
    }
    public String dateToString(Date date){
        return df.get().format(date);
    }
}