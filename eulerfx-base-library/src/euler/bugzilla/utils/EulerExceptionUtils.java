/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.utils;

import euler.bugzilla.dialogfx.DialogFX;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class EulerExceptionUtils {

    private static EulerExceptionUtils instance;  
    private static final Logger log = LoggerFactory.make();

    public String printTraceToString(Throwable exc) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exc.printStackTrace(pw);
        return sw.toString();
    }

    public  void handleException(Class clasz, Exception exc) {
        try {
            DialogFX dialog = new DialogFX(DialogFX.Type.ERROR);
            dialog.setMessage(printTraceToString(exc));
            dialog.setTitle(exc.getMessage());
            dialog.showDialog();
            log.error(Level.ERROR, exc);
        } catch (Exception ex) {
            log.error(Level.ERROR, ex);
        }
    }
   
    public static EulerExceptionUtils getInstance(){
        synchronized(EulerExceptionUtils.class){
            if(instance == null){
                instance = new EulerExceptionUtils();
            }
        }
        return instance;
    }

    public static class LoggerFactory {

        public LoggerFactory() {
        }

        public static Logger make() {
            Throwable t = new Throwable();
            StackTraceElement directCaller = t.getStackTrace()[1];
            return Logger.getLogger(directCaller.getClassName());
        }
    }
}
