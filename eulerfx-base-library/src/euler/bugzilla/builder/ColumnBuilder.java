/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.builder;

import java.util.Comparator;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

/**
 *
 * @author agime
 */
public interface ColumnBuilder<S, T> {

    public TableColumn createColumn(String propertyName);
    public TableColumn createColumn(String propertyName, Comparator comparator);
    public TableColumn createColumn(String propertyName, Callback callback);    
    public TableColumn createColumn(String propertyName, Comparator comparator, Callback callback);        
    
}
