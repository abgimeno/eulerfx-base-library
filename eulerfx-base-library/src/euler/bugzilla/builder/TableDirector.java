/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.builder;

import euler.bugzilla.comparator.BugIdComparator;
import euler.bugzilla.comparator.DateComparator;
import euler.bugzilla.comparator.SeverityComparator;
import euler.bugzilla.event.LoadBugCommentsEvent;
import euler.bugzilla.fx.table.BugTableView;
import euler.bugzilla.exception.QueryTableConfMissingException;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.conf.PositionComparator;
import euler.bugzilla.model.conf.QueryTableColumn;
import euler.bugzilla.model.conf.QueryTableConf;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import orius.event.RowClickEvent;
import orius.fx.cell.BugClickableListCell;
import orius.fx.cell.BugClickableStringCell;

/**
 *
 * @author Abraham Gimeno
 */
public class TableDirector {
    
    protected final BugInfoCellBuilder columnBuilder = new BugInfoCellBuilder();
            
    protected final DateComparator dateComparator = new DateComparator();
    protected final SeverityComparator severityComparator = new SeverityComparator();
    protected final BugIdComparator bugIdComparator = new BugIdComparator();
    
    protected final Callback<TableColumn<BugInfo, String>, TableCell<BugInfo, String>> clickableStringCellFactory
            = new Callback<TableColumn<BugInfo, String>, TableCell<BugInfo, String>>() {
                @Override
                public TableCell<BugInfo, String> call(TableColumn<BugInfo, String> p) {
                    return new BugClickableStringCell(new RowClickEvent());
                }
            };
    protected final Callback<TableColumn<BugInfo, List>, TableCell<BugInfo, List>> clickableListCellFactory
            = new Callback<TableColumn<BugInfo, List>, TableCell<BugInfo, List>>() {
                @Override
                public TableCell<BugInfo, List> call(TableColumn<BugInfo, List> p) {
                    return new BugClickableListCell(new RowClickEvent());
                }
            };
    
    protected final Callback<TableColumn<BugInfo, Button>, TableCell<BugInfo, Button>> buttonFactory 
            = new Callback<TableColumn<BugInfo, Button>, TableCell<BugInfo, Button>>() {

        @Override
        public TableCell<BugInfo, Button> call(TableColumn<BugInfo, Button> param) {
            return new  TableCell<BugInfo, Button>();//To change body of generated methods, choose Tools | Templates.
        }
    };    
    
    public BugTableView createTable() {
        final BugTableView table = new BugTableView();
        //initialiseTableWithDefaultConfiguration(table);
        return table;
    }

    public BugTableView createTable(QueryTableConf configuration) throws QueryTableConfMissingException {
        final BugTableView table = new BugTableView();
        if (configuration == null) {
            // initialiseTableWithDefaultConfiguration(table);
            throw new QueryTableConfMissingException("No configuration has been found. There should be always a configuration created automatically by the system. Check BugTableBean.java ");
        } else {
            useSetConfiguration(table, configuration);
        }

        // add listener to the tableview selecteditemproperty to copy the content to clipboard
        table.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object oldValue, Object newValue) {
                BugInfo bi = (BugInfo) newValue;
                LoadBugCommentsEvent lbce = new LoadBugCommentsEvent(Integer.parseInt(bi.getId()), LoadBugCommentsEvent.LOAD_BUGS);
                Event.fireEvent(table, lbce);
            }
        });
        table.autosize();
        table.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        table.setEditable(true);
        return table;
    }

    private void useSetConfiguration(BugTableView table, QueryTableConf configuration) {
        BugInfo bi = new BugInfo(); // I create a dummy object beacause there is no other way to know if a property is string or a list
        TableColumn col = new TableColumn();
        ObservableList list = FXCollections.<QueryTableColumn>observableArrayList();
        list.addAll(configuration.getColumns());
        SortedList<QueryTableColumn> columns = new SortedList<QueryTableColumn>(list);        
        columns.setComparator(new PositionComparator());
        for (QueryTableColumn qtc : columns) {
            Property p = bi.getInternalState().get(qtc.getName());
            if (p instanceof StringProperty) { // the type of data is a simple string
                if (BugInfo.isDate(qtc.getName()) || qtc.getName().equals(BugInfo.SEVERITY)) {
                    if (BugInfo.isDate(qtc.getName())) {
                        col = columnBuilder.createColumn(BugInfo.generatePropertyName(qtc.getName()), dateComparator, clickableStringCellFactory);
                    }
                    if (qtc.getName().equals(BugInfo.SEVERITY)) {
                        col = columnBuilder.createColumn(BugInfo.generatePropertyName(qtc.getName()), severityComparator, clickableStringCellFactory);
                    }
                } 
                else {
                    col = columnBuilder.createColumn(BugInfo.generatePropertyName(qtc.getName()), clickableStringCellFactory);
                }
            }

            if (p instanceof IntegerProperty) {
                col = columnBuilder.createColumn(BugInfo.generatePropertyName(qtc.getName()), bugIdComparator, clickableStringCellFactory);
            }
            if (p instanceof ListProperty) { // the type of data is a list (will concatenate values)
                col = columnBuilder.createColumn(BugInfo.generatePropertyName(qtc.getName()), clickableListCellFactory);
            }
            col.setVisible(qtc.getVisible());
            if (qtc.getWidth() > 0) {
                col.setPrefWidth(qtc.getWidth());
            }
            table.getColumns().add(col);
        }

    }    
}
