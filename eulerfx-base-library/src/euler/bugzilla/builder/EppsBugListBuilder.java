/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author agime
 */
public class EppsBugListBuilder {

    protected static List<String> products = new ArrayList<>();

    static {
        List<String> aList = new ArrayList<>();
        aList.add("CEPROC-PREPROD");
        aList.add("CEPROC-DEV");
        aList.add("CEPROC-TEST");
        aList.add("FFM/PDFGen XSD208s02");
        aList.add("LCC e-Procurement");
        aList.add("TfL e-Procurement");
        aList.add("YPO e-Procurement");
        aList.add("ePPS-base");
        aList.add("SCC e-Procurement");
        aList.add("Malta eProc");
        aList.add("FSA e-Procurement");
        aList.add("MAS eProc");
        aList.add("OECS eproc");
        aList.add("ESPO e-Procurement");
        aList.add("NI-eproc");
        products = Collections.unmodifiableList(aList);

    }
    ;    
   protected static List<String> statuses = new ArrayList<>();

    static {
        List<String> aList = new ArrayList<>();
        aList.add("NEW");
        aList.add("ASSIGNED");
        aList.add("REOPENED");
        statuses = Collections.unmodifiableList(aList);
    }
    ;    
   protected static List<String> priorityOneList = new ArrayList<>();

    static {
        List<String> aList = new ArrayList<>();
        aList.add("Critical Bug. Close Immediately");
        aList.add("Fix Before Deployment of Release");
        priorityOneList = Collections.unmodifiableList(aList);
    }
    ;  
   protected static List<String> priorityTwoList = new ArrayList<>();

    static {
        List<String> aList = new ArrayList<>();
        aList.add("Whenever possible");
        aList.add("New development");
        aList.add("Change request/ Question");
        aList.add("Initial requirement");
        priorityTwoList = Collections.unmodifiableList(aList);
    }
    ;   
     protected static List<String> products2Verify = new ArrayList<>();

    static {
        List<String> aList = new ArrayList<>();
        aList.add("LCC e-Procurement");
        aList.add("TfL e-Procurement");
        aList.add("YPO e-Procurement");
        aList.add("ePPS-base");
        aList.add("SCC e-Procurement");
        aList.add("Malta eProc");
        aList.add("FSA e-Procurement");
        aList.add("MAS eProc");
        aList.add("OECS eproc");
        aList.add("ESPO e-Procurement");
        aList.add("NI-eproc");
        products2Verify = Collections.unmodifiableList(aList);

    }
    ;
        /* this list is used both to get the propagation bugs and to 
         * reject propagation bugs from prio1 and prio2 lists
         */        
    
     protected static List<String> propagationSummary = new ArrayList<>();

    static {
        List<String> aList = new ArrayList<>();
        aList.add("Propagation of Bug");
        aList.add("Commit fix");
        aList.add("Commit flavour");
        propagationSummary = Collections.unmodifiableList(aList);
    }

    ;
    public static List<String> getProducts() {
        return products;
    }

    public static List<String> getStatuses() {
        return statuses;
    }

    public static List<String> getPriorityOne() {
        return priorityOneList;
    }

    public static List<String> getPriorityTwo() {
        return priorityTwoList;
    }

    public static List<String> getProducts2Verify() {
        return products2Verify;
    }
    
    public static List<String> getPropagationSummary(){
        return propagationSummary;
    }
}
