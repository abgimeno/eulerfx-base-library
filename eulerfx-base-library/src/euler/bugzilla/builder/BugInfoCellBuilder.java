/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.builder;

import euler.bugzilla.model.BugInfo;
import java.util.Comparator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 *
 * @author agime
 */
public class BugInfoCellBuilder implements ColumnBuilder {
 
    @Override
    public TableColumn createColumn(String propertyName) {        
        return createColumn(propertyName, null, null);
    }

    @Override
    public TableColumn createColumn(String propertyName, Comparator comparator) {
        return createColumn(propertyName, comparator, null);
    }   
    @Override
    public TableColumn createColumn(String propertyName, Callback callback) {
        return createColumn(propertyName, null, callback);
    }    
    @Override
    public TableColumn createColumn(String propertyName, Comparator comparator, Callback callback) {
        TableColumn<BugInfo, String> col = new TableColumn<>();
        col.setText(propertyName);
        col.setCellValueFactory(new PropertyValueFactory(propertyName));
        col.setCellFactory(callback);
        if (comparator != null) {
            col.setComparator(comparator);
        }
        if (callback != null){
            col.setCellFactory(callback);
        }
        col.setId(propertyName);        
        return col;
    }  
     
}
