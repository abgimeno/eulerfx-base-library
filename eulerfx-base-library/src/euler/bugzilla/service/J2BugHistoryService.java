/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.service;

import euler.bugzilla.j2bugzilla.history.BugHistoryInfo;
import euler.bugzilla.j2bugzilla.history.BugHistoryParameter;
import euler.bugzilla.j2bugzilla.history.BugHistory;
import euler.bugzilla.j2bugzilla.BugSearch;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.model.Bug;
import euler.bugzilla.model.query.QueryParameters;
import euler.bugzilla.model.query.SearchLimiter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 
 This class returns a List<Bug> and then, from those it removes
 the ones that do not match the BugHistoryParameter (iterative search within bug history is performed)
 are removed. This processing is done on memory due to limitations on the webservice.
 
 e.g. I cannot get all bugs fixed last week using a webservice, I can get all bugs fixed, that were
 changed after a particular date, but this still will return some that were fixed in another date and 
 recently changed another field that is not the resolution.
 * 
 */
public class J2BugHistoryService extends J2BugSearchService {

    private BugHistoryParameter bhp;
    
    public J2BugHistoryService(BugzillaConnector conn, BugHistoryParameter bhp, Map<SearchLimiter, List<String>> params) {
        super(conn, params);
        this.bhp=bhp;
    }
    
    @Override
    public List<Bug> getResults(Map<SearchLimiter, List<String>> params) {
        try {            
            QueryParameters[] sq = new QueryParameters[params.size()];
            List<QueryParameters> list = new ArrayList<>();
            for (SearchLimiter sl : params.keySet()){
                list.add(new QueryParameters(sl, params.get(sl)));
            }
            list.toArray(sq);
            BugSearch search = new BugSearch(sq);
            conn.executeMethod(search);
            List<Bug> bugs = search.getSearchResults();
            return bugs.isEmpty()? bugs: processHistory(bugs);
        } catch (BugzillaException ex) {
            log.error(null,ex);
            return null;
        }  
    }

    private Map<Integer, Bug> createMap(List<Bug> results) {
        Map<Integer, Bug> mappedBugs = new HashMap<>();
        for (Bug b : results) {
            mappedBugs.put(b.getID(), b);
        }
        return mappedBugs;
    }

    private List<Bug> processHistory(List<Bug> bugs) {
        try {
            Map<Integer, Bug> mappedBugs = createMap(bugs);
            log.debug("Starting to analyse history...");
            BugHistory mbh = new BugHistory(mappedBugs.keySet().toArray());
            conn.executeMethod(mbh);
            Map<Integer, BugHistoryInfo> history = mbh.getBugsHistory();
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            log.debug("Date: " + df.format(bhp.getDate()));
            int res = 0;
            for (Integer i : history.keySet()) {
                BugHistoryInfo bh = history.get(i);                           
                if (bh.searchOnAddedField(bhp)) {                     
                    res++;
                }else{
                    mappedBugs.remove(i);
                }
            }        
            log.info("Bugs matching history parameters: "+res);
            List<Bug> cleanedBugs= new ArrayList();
            cleanedBugs.addAll(mappedBugs.values());
            return cleanedBugs;
        } catch (BugzillaException ex) {
            log.error(null,ex);
            return null;
        }
    }

    
}
