/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.service;

import euler.bugzilla.j2bugzilla.BugComments;
import euler.bugzilla.model.Bug;
import euler.bugzilla.j2bugzilla.BugSearch;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.base.Comment;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.model.query.QueryParameters;
import euler.bugzilla.model.query.SearchLimiter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
/**
 *
 * @author agime
 */
public class J2BugSearchService extends AbstractJ2BugService {

    public J2BugSearchService(BugzillaConnector conn, Map<SearchLimiter, List<String>> params) {
        this(conn);
        this.params = params;
    }

    public J2BugSearchService(BugzillaConnector conn) {
        super(conn);
       log.debug("creating service");
    }

    @Override
    public List<Bug> getResults(Map<SearchLimiter, List<String>> params) throws Exception{       
            QueryParameters[] sq = new QueryParameters[params.size()];
            List<QueryParameters> list = new ArrayList<>();
            for (SearchLimiter sl : params.keySet()) {
                list.add(new QueryParameters(sl, params.get(sl)));
            }
            list.toArray(sq);
            BugSearch search = new BugSearch(sq);
            conn.executeMethod(search);
            return search.getSearchResults();
    }

    public List<Comment> getListOfComments(int bugId) {
        try {
            log.info("attempting to retrieve bug " + bugId);
            BugComments byID = new BugComments(bugId);
            conn.executeMethod(byID);
            return byID.getComments();
        } catch (BugzillaException ex) {
           Logger.getLogger(this.getClass()).error(null,ex);
        }
        return null;
    }
}
