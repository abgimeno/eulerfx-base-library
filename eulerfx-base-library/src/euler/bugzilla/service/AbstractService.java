/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.service;

import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.utils.EulerExceptionUtils;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public abstract class AbstractService extends Service<ObservableList>{
    
    protected BugzillaConnector conn;  
    protected static final Logger log = EulerExceptionUtils.LoggerFactory.make();
    public AbstractService(BugzillaConnector conn) {
        this.conn=conn;
    }
   
}
