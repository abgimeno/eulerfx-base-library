/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.service;

import euler.bugzilla.j2bugzilla.BugComments;
import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.base.Comment;
import java.util.List;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class CommentService extends AbstractService {

    private Integer bugId;

    public CommentService(Integer bugId, BugzillaConnector conn) {
        super(conn);
        this.bugId = bugId;
    }

    @Override
    protected Task<ObservableList> createTask() {
        return new GetCommentsTask();
    }


    public class GetCommentsTask extends Task<ObservableList> {

        @Override
        protected ObservableList call() {
            ObservableList<Comment> items = new SimpleListProperty(FXCollections.<Comment>observableArrayList());
            items.addAll(getListOfComments(bugId));
            return items;
        }

        public List<Comment> getListOfComments(int bugId) {
            try {
                log.info("Attempting to retrieve comments for bug " + bugId);
                BugComments byID = new BugComments(bugId);
                conn.executeMethod(byID);
                return byID.getComments();
            } catch (Exception ex) {
                Logger.getLogger(this.getClass()).error(null, ex);
            }
            return null;
        }
    }

}
