/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.service;

import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.rpc.GetAccessibleProducts;
import euler.bugzilla.j2bugzilla.base.Product;
import euler.bugzilla.j2bugzilla.enums.ProductParams;
import euler.bugzilla.j2bugzilla.rpc.GetProduct;
import euler.bugzilla.model.ProductInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class GetAccessibleProductsService extends AbstractService {

    private Map<ProductParams, List<String>> params = new HashMap<>();

    public GetAccessibleProductsService(BugzillaConnector conn, Map<ProductParams, List<String>> params) {
        super(conn);
        this.params = params;
    }

    @Override
    protected Task<ObservableList> createTask() {
        return new GetAccessibleProductsTask();
    }

    public class GetAccessibleProductsTask extends Task<ObservableList> {

        @Override
        protected ObservableList<ProductInfo> call() throws Exception {
            log.debug("GetAccessibleProductsTask called ...");
            ObservableList<ProductInfo> items = new SimpleListProperty(FXCollections.<ProductInfo>observableArrayList());
            GetAccessibleProducts get = new GetAccessibleProducts();
            conn.executeMethod(get);
            GetProduct gp = new GetProduct(get.getProductIDs(), params);
            conn.executeMethod(gp);
            for (Product p : gp.getProducts()) {
                items.add(new ProductInfo(p));
            }
            return items;
        }
    }
}
