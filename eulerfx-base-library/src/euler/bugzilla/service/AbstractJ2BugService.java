/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.service;

import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.model.Bug;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.model.query.SearchLimiter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public abstract class AbstractJ2BugService extends AbstractService{


    protected Map<SearchLimiter, List<String>> params = new HashMap<>();    
    
    /*Rejection reasons search in the bug values returned certain entries and
     * do not include them in the list of bugs if they are found*/    
    protected Map<SearchLimiter, List<String>> rejections = new HashMap();        
            
     //*Abstract functions*/            
    public abstract List<Bug> getResults(Map<SearchLimiter, List<String>> params) throws Exception;

    //*end of Abstract functions*/
    public AbstractJ2BugService(BugzillaConnector conn) {
        super(conn);
    }
    
    
   @Override
   public Task<ObservableList> createTask() {
        return new BugTask();
    }
   public class BugTask extends Task<ObservableList>{

       @Override
       protected ObservableList<BugInfo> call() throws Exception {           
           Logger.getLogger(this.getClass()).debug("Starting to execute the task ");
           ObservableList<BugInfo> items = new SimpleListProperty(FXCollections.<BugInfo>observableArrayList());
           List<Bug> bugs = getResults(params);
           if (bugs != null) {
               Logger.getLogger(this.getClass()).info(" -- Number of bugs found: " + bugs.size() + "\n");           
               for (Bug b : bugs) {
                  if(rejections.isEmpty() || !includesRejectionReason(b)){ // if rejection is empty, it enters directly                    
                       items.add(new BugInfo(b));
                  }
               }
           }
           return items;
       }

        private boolean includesRejectionReason(Bug b) {
            Map values = b.getParameterMap();  
            for(SearchLimiter sl : rejections.keySet()){
                for(String reason: rejections.get(sl)){
                    if(((String)values.get(sl.getName())).startsWith(reason)){
                        Logger.getLogger(this.getClass()).info("Rejecting " + values.get(sl.getName()));
                        return true;
                    }
                }                
            }
            return false;
        }        

   }

    public Map<SearchLimiter, List<String>> getRejections() {
        return rejections;
    }

    public void setRejections(Map<SearchLimiter, List<String>> rejections) {
        this.rejections = rejections;
    }
    
}
