/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.bugzilla.service;

import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.GetLegalValues;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.j2bugzilla.base.Product;
import euler.bugzilla.model.query.Fields;
import euler.bugzilla.utils.LegalValuesUtils;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public class GetLegalValuesService extends AbstractService {

    private Fields field;
    private Product product=null;
    
    public GetLegalValuesService(BugzillaConnector conn, Fields field) {
        super(conn);
        this.field=field;       
    }   
    
    public GetLegalValuesService(BugzillaConnector conn, Fields field, Product product) {
        this(conn, field);
        this.product=product;
    }

    public Fields getField() {
        return field;
    }

    public void setField(Fields field) {
        this.field = field;
    }

    @Override
    protected Task<ObservableList> createTask() {
        return new GetLegalValuesTask();
    }

    public class GetLegalValuesTask extends Task<ObservableList> {

        @Override
        protected ObservableList call() {
            ObservableList<String> items = new SimpleListProperty(FXCollections.<String>observableArrayList());
            try {
                Logger.getLogger(this.getClass()).debug("Starting to execute the task for "+field.getInternalName());
                GetLegalValues glv = (LegalValuesUtils.filterResults(field) && product != null)? 
                                                     new GetLegalValues(field.getInternalName(), product)
                                                    :new GetLegalValues(field.getInternalName());
                conn.executeMethod(glv);
                items.addAll(glv.getLegalValues());
                Logger.getLogger(this.getClass()).info(items.size()+ " legal values found for "+field.getInternalName());
            } catch (BugzillaException ex) {
                Logger.getLogger(this.getClass()).error(field.getInternalName(),ex);
            }
            return items;
        }
    }
}
