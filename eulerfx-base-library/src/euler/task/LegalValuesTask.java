/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.task;

import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.GetLegalValues;
import euler.bugzilla.j2bugzilla.base.BugzillaException;
import euler.bugzilla.j2bugzilla.base.Product;
import euler.bugzilla.model.ProductInfo;
import euler.bugzilla.model.query.Fields;
import euler.bugzilla.model.query.LegalValue;
import euler.bugzilla.utils.LegalValuesUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javafx.concurrent.Task;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public abstract class LegalValuesTask extends Task<Collection<LegalValue>> {

    protected BugzillaConnector conn;
    private final List<ProductInfo> products;

    public LegalValuesTask(BugzillaConnector conn, List<ProductInfo> products) {
        this.conn = conn;
        this.products = products;
    }

    public abstract void persistLegalValue(LegalValue legalValue);
    public abstract List<LegalValue> getLegalValues(Fields field, ProductInfo product);
    public abstract List<LegalValue> getLegalValues(Fields field);

    @Override
    protected Collection<LegalValue> call() throws Exception {
        Logger.getLogger(this.getClass()).debug("Starting to execute the task");
        Collection<LegalValue> results = new ArrayList<>();
        try {
            for (Fields field : Fields.values()) {
                GetLegalValues glv = null;
                if (LegalValuesUtils.filterResults(field)) {
                    handleLegalValuesDependOnProduct(glv, field, results);
                } else {
                    handleLegalValuesDontDependOnProduct(glv, field, results);
                }
            }
        } catch (BugzillaException ex) {
            Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
        }
        return results;

    }

    private void handleLegalValuesDependOnProduct(GetLegalValues glv, Fields field, Collection<LegalValue> results) throws BugzillaException {
        for (ProductInfo pi : products) {
            Product p = new Product(pi.getId(), pi.getName());
            glv = new GetLegalValues(field.getInternalName(), p);
            conn.executeMethod(glv);
            List<LegalValue> legalValues = getLegalValues(field, pi);
            if (legalValues.isEmpty()) {
                LegalValue legalValue = setDataForLegalValue(glv, field, pi, results);
                persistLegalValue(legalValue);
                results.add(legalValue);
            }
        }
    }

    private void handleLegalValuesDontDependOnProduct(GetLegalValues glv, Fields field, Collection<LegalValue> results) throws BugzillaException {
        glv = new GetLegalValues(field.getInternalName());
        conn.executeMethod(glv);        
        List<LegalValue> legalValues = getLegalValues(field);
        if (legalValues.isEmpty()) {
            LegalValue legalValue = setDataForLegalValue(glv, field, results);
            persistLegalValue(legalValue);
            results.add(legalValue);
        }
        Logger.getLogger(this.getClass()).info(glv.getLegalValues().size() + " legal values found for " + field.getInternalName());
    }
    private LegalValue setDataForLegalValue(GetLegalValues glv, Fields field, Collection<LegalValue> results) {
        return setDataForLegalValue(glv, field, null, results);
    }

    private LegalValue setDataForLegalValue(GetLegalValues glv, Fields field, ProductInfo pi, Collection<LegalValue> results) {
        LegalValue legalValue = new LegalValue();
        legalValue.setField(field);
        List<String> legalValues = new ArrayList<>();
        legalValues.addAll(glv.getLegalValues());
        legalValue.setLegalValues(legalValues);
        legalValue.setProduct(pi);
        results.add(legalValue);
        StringBuilder sb = new StringBuilder(pi==null?"":new StringBuilder("Product").append(pi.getName()));
        Logger.getLogger(this.getClass()).info(sb.append("->").append(glv.getLegalValues().size()).append(" legal values found for ").append(field.getInternalName()).toString());
        return legalValue;
    }


}
