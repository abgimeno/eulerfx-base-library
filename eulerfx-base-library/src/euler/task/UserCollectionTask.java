/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.task;

import euler.bugzilla.j2bugzilla.BugzillaConnector;
import euler.bugzilla.j2bugzilla.enums.UserInfoParams;
import euler.bugzilla.j2bugzilla.user.UserInfo;
import euler.bugzilla.model.BugzillaUser;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.concurrent.Task;
import org.apache.log4j.Logger;

/**
 *
 * @author agime
 */
public abstract class UserCollectionTask extends Task<Collection<BugzillaUser>> {

    protected BugzillaConnector conn;

    public UserCollectionTask(BugzillaConnector conn) {
        this.conn = conn;
    }

    public abstract void persistUser(BugzillaUser user);

    @Override
    protected Collection<BugzillaUser> call() throws Exception {
        Logger.getLogger(this.getClass()).debug("Starting to execute the task");
        List<BugzillaUser> users = new ArrayList<>();
        Map<UserInfoParams, List<String>> parameters = new HashMap<>();
        List<String> keys = new ArrayList<>();
        keys.add("*");
        parameters.put(UserInfoParams.MATCH, keys);
        keys = new ArrayList<>();
        keys.add("id");
        keys.add("name");
        keys.add("real_name");
        keys.add("email");
        parameters.put(UserInfoParams.INCLUDE, keys);
        UserInfo method = new UserInfo(parameters);
        conn.executeMethod(method);
        Logger.getLogger(this.getClass()).debug(method.getResults().size() + " users found");
        for (BugzillaUser bu : method.getResults()) {
            persistUser(bu);
        }
        return users;
    }
}
