/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx8.collections;

import java.util.function.Predicate;

/**
 *
 * @author agime
 */
public class AlwaysTrue implements Predicate<Boolean> {

   
    @Override
    public boolean test(Boolean t) {
        return true;
    }

    
    public boolean negate(Boolean e) {
        return  true;
    }

    
    public boolean and(Boolean e, Predicate<? super Boolean> other) {
        return  true;
    }

    
    public boolean or(Boolean e, Predicate<? super Boolean> other) {
        return  true;
    }

    
}
