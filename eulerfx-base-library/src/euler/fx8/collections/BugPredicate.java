/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx8.collections;

import euler.bugzilla.model.BugInfo;
import static euler.bugzilla.model.BugInfo.DATEFORMAT;
import euler.bugzilla.utils.ConcurrentDateFormatAccess;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import javafx.beans.property.ListProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author abrahamgimeno
 */
public class BugPredicate implements Predicate<BugInfo> {

    private final String key;
    private final String value;

    /*
     @key is the field name in the bug
     @value is the value I will compare against
     */
    public BugPredicate(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean test(BugInfo t) {
        ConcurrentDateFormatAccess df = new ConcurrentDateFormatAccess(DATEFORMAT);
        Map bugMap = t.getInternalState();
        if (bugMap.get(key) instanceof StringProperty) {
            StringProperty sp = (StringProperty) bugMap.get(key);
            return sp.get().startsWith(value);
        }
        if (bugMap.get(key) instanceof ListProperty) {
            return false; //TO-DO support for arrays, I don't think it's worth
        }
        return false;
    }

    public boolean and(BugInfo e, Predicate<? super BugInfo> other) {
        return test(e) && other.test(e);
    }

    public boolean negate(BugInfo e) {
        return !test(e);
    }

    public boolean or(BugInfo e, Predicate<? super BugInfo> other) {
        return test(e) || other.test(e);
    }

}
