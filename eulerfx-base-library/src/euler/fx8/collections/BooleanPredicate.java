/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.fx8.collections;

import java.util.function.Predicate;

/**
 *
 * @author agime
 */
public class BooleanPredicate implements Predicate<Boolean> {
     
    
    @Override
    public boolean test(Boolean t) {
        return t;
    }


    public boolean negate(Boolean e) {
        return !e;
    }


  
    public boolean and(Boolean e, Predicate<? super Boolean> other) {        
        return test(e) && other.test(e);
    }

   
    public boolean or(Boolean e, Predicate<? super Boolean> other) {
        return test(e) || other.test(e);
    }


    
}
