/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package orius.fx.cell;

/**
 *
 * @author agime
 */


import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import orius.fx.ProductSelectedEvent;

/**
 * this class is used when the user is selecting the products
 * needs to be made generic
 * @author agime
 */
public class RadioButtonTableCell<S, T> extends TableCell<S, T> {

        private final RadioButton radioButton;

        public RadioButtonTableCell() {            
            radioButton = new RadioButton();
            radioButton.setAlignment(Pos.CENTER);            
            radioButton.setSelected(false);
            setAlignment(Pos.CENTER);
            setGraphic(radioButton);               
            radioButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    ProductSelectedEvent pse = new ProductSelectedEvent(ProductSelectedEvent.PROD_SELECTED);
                    pse.setRow(getIndex()); //passing as parameter the selected 
                    Event.fireEvent(t.getTarget(), pse);                }
            });
        }

        @Override
        public void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                setGraphic(radioButton);
            }
        }
    }

