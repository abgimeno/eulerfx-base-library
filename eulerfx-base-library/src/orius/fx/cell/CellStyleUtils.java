/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.fx.cell;

import euler.bugzilla.model.BugInfo;
import javafx.scene.control.TableCell;

/**
 *
 * @author agime
 */
public class CellStyleUtils {

    public static <T extends TableCell> void setStyle(T cell, BugInfo bi) {
        
        if (bi.getOpenedThisWeek()) {
            cell.setStyle("-fx-background-color:linear-gradient(to bottom, derive(rgb(230,236,129),40%), derive(rgb(248,243,54),-40%));");            
        }
        if(bi.getModifiedThisWeek() && !bi.getOpenedThisWeek()){
                cell.setStyle("-fx-background-color:linear-gradient(to bottom, derive(rgb(213,226,248),20%), derive(rgb(179,204,255),-20%));");
        }        
        if (!bi.getOpenedThisWeek() && !bi.getModifiedThisWeek()) {
           cell.setStyle(""); // this removes the style, style needs to be removed because visible cells are reused
        }        
    }     
}
