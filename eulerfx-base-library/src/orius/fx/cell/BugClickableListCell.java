/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.fx.cell;

import euler.bugzilla.model.BugInfo;
import euler.fx.table.cell.ClickableListCell;
import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 *
 * @author agime
 */
public class BugClickableListCell extends ClickableListCell<BugInfo> {

    public BugClickableListCell(EventHandler<MouseEvent> mouseClickedEvent) {
        super(mouseClickedEvent);
    }

    @Override
    protected void updateItem(List t, boolean bln) {
        super.updateItem(t, bln); //To change body of generated methods, choose Tools | Templates.
            if(checkNulls() && checkBoundaries()){         
                try{
                    BugInfo bi = (BugInfo) this.getTableView().getItems().get(this.getTableRow().getIndex());
                    CellStyleUtils.setStyle(this, bi);
                }catch (IndexOutOfBoundsException ex){                    
                    Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
                    
                }
            }
    }

    private boolean checkNulls() {
        return this.getTableRow()!=null && getTableView() != null && getTableView().getItems() != null;
    }
    private boolean checkBoundaries() {
        return this.getTableRow().getIndex() < getTableView().getItems().size() && this.getTableRow().getIndex() >= 0;
    }
}
