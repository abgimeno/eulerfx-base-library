/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.fx.cell;

import javafx.event.EventHandler;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Abraham Gimeno
 */
public class ClickableHyperLinkCell<T> extends TableCell<T, Hyperlink>{

    public ClickableHyperLinkCell(EventHandler<MouseEvent> mouseClickedEvent) {
        super();
        addEventFilter(MouseEvent.MOUSE_CLICKED, mouseClickedEvent);
    }

    @Override
    protected void updateItem(Hyperlink t, boolean empty) {
        super.updateItem(t, empty); //To change body of generated methods, choose Tools | Templates.
        
        if(empty){// this removes old rows and custom CSS styles when new items has less rows than the old items stored in the table
            setText(null);
            setStyle(null);
            setGraphic(null);
        } 
        if (t != null) {
            //setText(t.getText());            
            setGraphic(t.getGraphic());
        }

    }    
}
