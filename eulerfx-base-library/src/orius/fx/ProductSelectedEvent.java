/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package orius.fx;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author agime
 */
public class ProductSelectedEvent extends Event{
    
    public static final EventType<ProductSelectedEvent> PROD_SELECTED = new EventType<>(Event.ANY, "SELECTED");        
    
    private Integer row;

    public ProductSelectedEvent(EventType<? extends Event> et) {
        super(et);
    }

    public ProductSelectedEvent(Object o, EventTarget et, EventType<? extends Event> et1) {
        super(o, et, et1);
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }
    
    @Override
    public ProductSelectedEvent copyFor(Object o, EventTarget et) {
        return (ProductSelectedEvent)super.copyFor(o, et); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EventType<? extends ProductSelectedEvent> getEventType() {
        return (EventType<? extends ProductSelectedEvent>)super.getEventType(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
