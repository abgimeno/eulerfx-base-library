/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.event;

import euler.bugzilla.model.query.Query;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;
import javafx.scene.control.TableColumn;

/**
 *
 * @author agime
 */
public class ChangeColumnWidthEvent extends Event{
    
    public static final EventType<ChangeColumnWidthEvent> CHANGED = new EventType<>(Event.ANY, "WIDTH_CHANGED");
    
    private final Double width;
    private final TableColumn tableColumn;
    private final Query query;
    
    public ChangeColumnWidthEvent(Double newWidth, Query query ,TableColumn tc , EventType<? extends Event> et) {
        super(et);
        this.width=newWidth;
        this.tableColumn=tc;
        this.query = query;
    }

    public ChangeColumnWidthEvent(Double newWidth, Query query, TableColumn tc , EventTarget et, EventType<? extends Event> et1) {
        super(newWidth, et, et1);
        this.width=newWidth;
        this.tableColumn=tc;
        this.query = query;
    }

    public Double getWidth() {
        return width;
    }

    public TableColumn getTableColumn() {
        return tableColumn;
    }

    public Query getQuery() {
        return query;
    }



    
    
}
