/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package orius.event;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author agime
 */
public class FolderEvent extends Event{
 
    public static final EventType<FolderEvent> FOLDER_CREATED = new EventType<>(Event.ANY, "FOLDER_CREATED");
    public static final EventType<FolderEvent> DELETE_FOLDER = new EventType<>(Event.ANY, "DELETE_FOLDER");
    private static final EventType<FolderEvent> NONE = new EventType<>(Event.ANY, "NONE");
    

    public FolderEvent(EventType<? extends Event> et) {
        super(et);
    }

    public FolderEvent() {
        super(ANY);
    }

    public FolderEvent(Object o, EventTarget et, EventType<? extends FolderEvent> et1) {
        super(o, et, et1);
    }

    @Override
    public FolderEvent copyFor(Object o, EventTarget et) {
        return (FolderEvent)super.copyFor(o, et); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EventType<? extends FolderEvent> getEventType() {
        return (EventType<? extends FolderEvent>)super.getEventType(); //To change body of generated methods, choose Tools | Templates.
    }
               
}
