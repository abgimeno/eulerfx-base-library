/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.event;

import euler.bugzilla.event.EditBugEvent;
import euler.bugzilla.exception.TableColumnNotFoundException;
import euler.fx.table.cell.ClickableStringCell;
import euler.bugzilla.model.BugInfo;
import euler.bugzilla.fx.table.BugTableView;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import orius.fx.cell.ClickableHyperLinkCell;

/**
 *
 * @author agime
 */
public class RowClickEvent implements EventHandler<MouseEvent> {

    public RowClickEvent() {
        super();
    }

    @Override
    public void handle(MouseEvent t) {
     
        if (t.getClickCount() > 1) {
            try {
                /* getting source data, what row and bug called the fucntion
                * please note the 'get(0)' hard-coded below, at some point it has to be made dynamic
                */
                if(t.getSource() instanceof ClickableStringCell){
                    ClickableStringCell cell = (ClickableStringCell) t.getSource();
                    //TO-DO: the below is hard-coded and will not work if id column is relocated
                    BugTableView table = (BugTableView) cell.getTableView();
                    Object cellData = table.getColumn(BugInfo.ID).getCellData(cell.getIndex());
                    EditBugEvent lbce = new EditBugEvent(Integer.parseInt(cellData.toString()), EditBugEvent.EDIT);
                    Event.fireEvent(t.getTarget(), lbce);
                }
            } catch (TableColumnNotFoundException ex) {
                Logger.getLogger(this.getClass()).error(Level.ERROR, ex);
            }

        }

    }

}
