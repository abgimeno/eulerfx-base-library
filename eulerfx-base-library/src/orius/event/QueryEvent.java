/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package orius.event;

import euler.bugzilla.model.query.Query;
import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author agime
 */
public class QueryEvent extends Event{
    
    public static final EventType<QueryEvent> EXECUTE = new EventType<>(Event.ANY, "EXECUTE");
    public static final EventType<QueryEvent> SYNCRONISE = new EventType<>(Event.ANY, "SYNCRONISE");
    public static final EventType<QueryEvent> SYNCRONISE_DONE = new EventType<>(Event.ANY, "SYNCRONISE_DONE");    
    public static final EventType<QueryEvent> DELETE = new EventType<>(Event.ANY, "DELETE");
    public static final EventType<QueryEvent> CONFIGURE_COLUMNS = new EventType<>(Event.ANY, "CONFIGURE_COLUMNS");
    public static final EventType<QueryEvent> OPEN_COL_CONF = new EventType<>(Event.ANY,"OPEN_COL_CONF");

    private final Query query;
    

    public QueryEvent(Query query, EventType<? extends QueryEvent> et) {
        super(et);
        this.query=query;
    }

    public QueryEvent(Query query, EventTarget et, EventType<? extends QueryEvent> et1) {
        super(et1);
        this.query=query;
    }

    @Override
    public QueryEvent copyFor(Object o, EventTarget et) {
        return (QueryEvent)super.copyFor(o, et); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public EventType<? extends QueryEvent> getEventType() {
        return (EventType<? extends QueryEvent>)super.getEventType(); //To change body of generated methods, choose Tools | Templates.
    }

    public Query getQuery() {
        return query;
    }
           

   
}
